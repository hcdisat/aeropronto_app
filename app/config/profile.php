<?php

return [

    /*
     * Set months format. dont' chane
     * the key of the array just the format
     */
    'months' => [
        '01' => 'ENE (01)',
        '02' => 'FEB (02)',
        '03' => 'MAR (03)',
        '04' => 'ABR (04)',
        '05' => 'MAY (05)',
        '06' => 'JUN (06)',
        '07' => 'JUL (07)',
        '08' => 'AGO (08)',
        '09' => 'SEP (09)',
        '10' => 'OCT (10)',
        '11' => 'NOV (11)',
        '12' => 'DIC (12)',
    ],

    /*
     * Generates years from current year to this year plus 20
     */
    'years' => call_user_func(function(){
        $now = \Carbon\Carbon::now()->year;
        $years = [];
        $limit = $now + 40;

        for($i = $now; $i <= $limit; $i++){
            $years[substr($i, 2)] = $i;
        }

        return $years;
    })
];