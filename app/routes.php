<?php

use DisatCorp\Listeners\EmailNotifier;

Event::listen('DisatCorp.*', EmailNotifier::class);

Event::listen('DisatCorp.Events.BillWasCreated', function($event)
{
    var_dump('Send a notification email to the job creator.');
});

    $routes_path = app_path().'/routes';

    Route::when('*', 'csrf', ['post', 'put', 'patch']);
    Route::controller('password', 'RemindersController');


    foreach(File::files($routes_path) as $file) {
        require_once $file;
    }