<?php

class Status extends \Eloquent {
	protected $fillable = ['name', 'abbr', 'css_class', 'description'];

    public static function getStatusByName($name)
    {
        return Status::whereName($name)
            ->get();
    }
}