<?php

class Role extends \Eloquent {

	protected $fillable = ['name', 'description'];


	public function users()
	{
		return $this->belongsToMany(User::class);
	}

}