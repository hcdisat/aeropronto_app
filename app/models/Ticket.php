<?php

class Ticket extends \Eloquent
{
	protected $guarded = ['id'];

	/**
	 * @return mixed
	 */
	public function status()
	{
		return $this->belongsTo(TicketStatus::class, 'ticket_status_id');
	}

	public function priority()
	{
		return $this->belongsTo(TicketPriority::class, 'ticket_priority_id');
	}

	public function package()
	{
		return $this->belongsTo(Package::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function users()
	{
		return $this->belongsToMany(User::class);
	}

	public function comments()
	{
		return $this->hasMany(TicketComment::class);
	}
}