<?php

class Item extends \Eloquent {
	protected $fillable = [
        'package_id', 'weight',
        'name', 'description',
        'price'
    ];
}