<?php

use DisatCorp\Api\Packages\NotificationMailTrait,
    \DisatCorp\Api\Packages\PackagesTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Package extends \Eloquent {
    use NotificationMailTrait, PackagesTrait, SoftDeletingTrait;

	protected $fillable = [
        'user_id', 'payment', 'notes',
        'total_weight', 'status_id', 'tracking'
    ];

    protected $dates = ['deleted_at'];

    public static function boot()
    {
        parent::boot();

        static::created(function($package){
            $package->tracking = 'P-SDQ-0'.$package->id;
            $package->save();
        });

        static::created(function($package){
            if($package->status->order == 1) {
                self::onReceivedPackage($package);
            }
        });

        static::saved(function($package){
            if($package->status->order == 6){
                self::onReadyStatusPackage($package);
            }
        });
    }

    public function items()
    {
        return $this->hasMany('Item');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function status()
    {
        return $this->belongsTo('Status');
    }

    public function statuses()
    {
        return $this->belongsToMany('Status');
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function hasItems()
    {
        return $this->items->count() > 0;
    }
}