<?php

class Bill extends \Eloquent
{


	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function deliverMethod()
	{
		return $this->belongsTo(DeliverMethod::class);
	}


	public function billLines()
	{
		return $this->hasMany(BillLine::class);
	}

	public function package()
	{
		return $this->belongsTo(Package::class);
	}


	public static function boot()
	{
		self::created(function($bill){
			$bill->code = 'F-PSDQ-'.$bill->id;
//			$bill->total = $bill->deliverMethod->charge + Configuration::getConfig('taxes');
			$bill->save();
		});
		parent::boot();
	}
}