<?php

use DisatCorp\Api\Helpdesk\HelpdeskTrait;
use DisatCorp\Api\Permissions\PermissionTrait;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use \Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait, PermissionTrait, SoftDeletingTrait, HelpdeskTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $fillable = [
        'fname', 'lname', 'password',
        'email', 'confirmation_code'
    ];

    protected $dates = ['deleted_at'];


    protected $softDelete = true;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function getFullName()
    {
        return "{$this->attributes['lname']}, {$this->attributes['fname']}";
    }

    public function getFullNameAttribute()
    {
        return $this->getFullName();
    }

    public function roles()
    {
        return $this->belongsToMany('Role');
    }

    public function clienTickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function asignedTickets()
    {
        return $this->belongsToMany(Ticket::class);
    }

    public function packages()
    {
        return $this->hasMany(Package::class);
    }


    public static function getAdmins()
    {
        return Role::whereName('admin')
            ->first()
            ->users()->get();
    }


    public static function scopeClientService($query)
    {
        return $query->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', 2)
            ->orWhere('role_user.role_id', 4)
            ->get();
    }

    // Profile Relations

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function phones()
    {
        return $this->hasMany(Phone::class);
    }

    public function cards()
    {
        return $this->hasMany(Card::class);
    }


}
