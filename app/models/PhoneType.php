<?php

class PhoneType extends \Eloquent
{

    protected $guarded = ['id'];

    public function phones()
    {
        return $this->hasMany(Phone::class);
    }
}