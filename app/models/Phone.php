<?php

class Phone extends \Eloquent
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(PhoneType::class, 'phone_type_id');
    }
}