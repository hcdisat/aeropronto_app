<?php

use DisatCorp\Events\CommentWasPosted;
use Laracasts\Commander\Events\EventGenerator;

class TicketComment extends \Eloquent
{
	use EventGenerator;

	protected $guarded = ['id'];

	/**
	 * @return mixed
	 */
	public function ticket()
	{
		return $this->belongsTo(Ticket::class);

	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public static function post(array $data)
	{
		$comment = self::create($data);
		$comment->raise(new CommentWasPosted($comment));

		return $comment;
	}
}