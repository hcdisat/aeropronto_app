<?php

class Profile extends \Eloquent {
	protected $fillable = [
        'address', 'user_id', 'email',
        'phone_one', 'phone_two', 'city'
    ];

    public function user()
    {
        return $this->belongsTo('User');
    }
}