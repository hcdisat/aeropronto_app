<?php

class Configuration extends \Eloquent {
	protected $fillable = ['name', 'value', 'description'];

    public static function getPricePerPound() {
        return Configuration::whereName('peso')
            ->get(['id', 'value'])
            ->first();
    }

    public static function getConfig($name)
    {
        return Configuration::whereName($name)->first();
    }

    public static function updateConfig($name, $value)
    {
        $cnf = self::getConfig($name);
        $value = ['value' => $value];
        $cnf->update($value);
    }
}