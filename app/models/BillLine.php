<?php

class BillLine extends \Eloquent
{
	protected $guarded = ['id'];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function package()
	{
		return $this->belongsTo(Package::class);
	}

}