<?php

class Card extends \Eloquent
{
	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function getFullNameAttribute()
	{
		return $this->attributes->first_name.' '.$this->last_name;
	}

	public function getExpiratonAttribute()
	{
		return $this->expiration_month.'/'.$this->expiration_year;
	}

	public static function boot()
	{
		self::created(function($card){

			self::finalizeCard($card);
		});

		parent::boot();
	}

	/**
	 * Store he card correctly
	 * @param $card
	 */
	public static function finalizeCard($card)
	{
		$card->code = $card->last_four;
		$card->last_four = substr($card->code, strlen($card->code) - 4);

		for ($i = strlen($card->code) - 4; $i > 0; $i--) {
			$card->last_four = '*' . $card->last_four;
		}

		$card->save();
	}
}