<?php

class ConfigurationsController extends \BaseController {

	public function index()
	{
        $we = Configuration::getConfig('we');;
        $mission = Configuration::getConfig('mission');
        $values = Configuration::getConfig('values');
        $vision = Configuration::getConfig('vision');
        $profile = Profile::first();
        $subjects = TicketSubject::all();

        return View::make('configurations.index')
            ->withWeight(Configuration::getPricePerPound()->value)
            ->withWe($we)->withMission($mission)
            ->withValues($values)->withVision($vision)
            ->withContact($profile)
            ->withSubjects($subjects)
            ->withId(Configuration::getPricePerPound()->id);
	}

	public function update()
	{
        if(Input::get('id') > 0 && is_numeric(Input::get('value'))){
            $cnf = Configuration::find(Input::get('id'));
            if($cnf){
                $cnf->update(Input::all());
            }
        }
		return Redirect::back();
	}

    public function whoWeAre()
    {
        return $this->updateConfiguration('we');
    }

    public function mission()
    {
        return $this->updateConfiguration('mission');
    }

    public function vision()
    {
        return $this->updateConfiguration('vision');
    }

    public function values()
    {
        return $this->updateConfiguration('values');
    }

    private function updateConfiguration($cnf)
    {
        if(Input::has('value')) {
            Configuration::updateConfig($cnf, Input::get('value'));
            return Redirect::back();
        }
    }

    public function showAlerts($level, $message)
    {
        return View::make('components.dynamic-alert')
            ->withLevel($level)
            ->withMessage($message);
    }



    // subjects
    public function editSubject($subject_id)
    {
        $subject = TicketSubject::findOrFail($subject_id);
        return View::make('configurations.subjects.add')
            ->withSubject($subject);
    }

    public function addSubject()
    {
        return View::make('configurations.subjects.add');
    }

    public function storeSubject()
    {
        if( !Input::has('subject') ){
            Flash::error('No se pudo guardar el asunto.');
            return Redirect::back()->withInput(Input::all());
        }

        TicketSubject::create(Input::all());

        Flash::success('Asunto agregado correctamente.');
        return Redirect::route('settings.index');
    }

    public function updateSubject($subject_id)
    {
        if( !Input::has('subject') ){
            Flash::error('No se pudo actualizar el asunto.');
            return Redirect::back()->withInput(Input::all());
        }

        $subject = TicketSubject::findOrFail($subject_id);
        $subject->update(Input::all());

        Flash::success('Asunto acualizado correctamente.');
        return Redirect::route('settings.index');
    }

    public function deleteSubject($subject_id)
    {
        $subject = TicketSubject::findOrFail($subject_id);
        $subject->delete();

        Flash::success('Asunto borrado correctamen.');
        return Redirect::route('settings.index');
    }


}