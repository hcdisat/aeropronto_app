<?php

use DisatCorp\Commands\NewCommentCommand;

class CommentsController extends \BaseController {

	use \Laracasts\Commander\CommanderTrait;
	/**
	 * Store a newly created resource in storage.
	 * POST /comments
	 *
	 * @return Response
	 */
	public function store()
	{
		if( empty(Input::get('comment')) )
        {
            if( Request::ajax() ){
                abort(402);
            }
            return \Illuminate\Support\Facades\Redirect::back()
                ->withInput(Input::all());
        }

		$comment = $this->execute(NewCommentCommand::class);


        return Request::ajax()
            ? ['data' => HTML::showComment($comment)]
            : Redirect::back();
	}

}