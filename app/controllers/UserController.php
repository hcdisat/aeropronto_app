<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /user
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('configurations.users.index')
            ->withUsers(User::all());
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /user/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if($id > 0) {
            $user = User::find($id);
        }

        if($user) {
            return View::make('configurations.users.edit')
                ->withRoles(Role::orderBy('id', 'DESC')->lists('name', 'id'))
                ->withUser($user);
        }

        return Redirect::back();
	}

    /**
     * Update the specified resource in storage.
     * PUT /user/{id}
     *
     * @internal param int $id
     * @return Response
     */
	public function update()
	{
        if(Input::has('user_id')) {
            $user = User::find(Input::get('user_id'));
            $user->update(Input::all());
            $user->revokeAllRoles();
            $user->addRoles(Input::get('add_roles'));
            $user->save();
            Flash::success("Usuario: {$user->getFullName()} modificado.");
            return Redirect::route('user.index');
        }

        Flash::success("Error Actualizando al usuario.");
        return Redirect::back()
            ->withInputs(Input::all());
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
        if($user) {
            $user->delete();
        }
	}

    public function findByName() {
        return User::get(['id', 'email as name']);
    }

}