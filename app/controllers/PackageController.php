<?php

use Carbon\Carbon;
use DisatCorp\Api\Reports\ReportableTrait;
use DisatCorp\Commands\AddItemCommand;
use DisatCorp\Forms\ItemForm;
use Laracasts\Commander\CommanderTrait;

class PackageController extends \BaseController
{
    use ReportableTrait, CommanderTrait;


    protected $itemForm;

    function __construct(ItemForm $itemForm)
    {
        $this->itemForm = $itemForm;
    }


    /**
     * Display a listing of the resource.
     * GET /package
     *
     * @return Response
     */
    public function index()
    {
        $statuses = Status::orderBy('id')->lists('name', 'id');
        return View::make('packages.home')
            ->withStatuses($statuses)
            ->withPackages(Package::withTrashed()->get());
    }

    /**
     * Show the form for creating a new resource.
     * GET /package/create
     *
     * @return Response
     */
    public function create()
    {
        return View::make('packages.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /package
     *
     * @return Response
     */
    public function store()
    {
        $package = null;
        $this->itemForm->validate(Input::all());

        if(Input::has('user_id')) {
            $package = Package::create(array_merge(Input::all(), ['status_id' => 1]));
            $package->statuses()->attach(1);
            $this->execute(AddItemCommand::class, array_merge(Input::all(), ['package_id' => $package->id]));
        }


        if($package) {
            Flash::success("Paquete <b>{$package->tracking}</b> creado correctamente");
            return Redirect::route('package.index');
        }


        Flash::error('El cliente no existe.');
        return Redirect::back()->withInputs(Input::all());
    }


    /**
     * Remove the specified resource from storage.
     * DELETE /package/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $target = Package::withTrashed()->find($id);
        $target->status_id = 9;
        $target->save();
        if($target->delete()){
            //Flash::message('Paquete borrado correctamente');
            return ['data' => null, 'error' => false, 'message' => 'success'];
        }

        //Flash::error('Paquete no borrado :( trate más tarde');
        return ['data' => null, 'error' => true, 'message' => 'ERROR'];
    }

    public function update($id) {

        $status_id = Input::get('status');
        $package = Package::withTrashed()->find(Input::get('id'));
        if($package)
        {
            if($package->updateStatuses($status_id))
            {
                $package->status_id = $status_id
                ;
                if( $status_id == 9 )
                    $package->deleted_at = null;

                $package->save();
                Flash::message("Estado cambiado a \"{$package->status->name}\"");
                return  Redirect::route('package.index');
            }
        }

        Flash::error('Error Actualizando el estado del paquete');
        return  Redirect::back()->withInput(Input::all());
    }

    public function show($id)
    {
        $package = Package::withTrashed()->find($id);
        $payment = $package->items->sum('price');
        $weight = $package->items->sum('weight');
        $paymethod = 'Personal';
        return View::make('packages.show')
            ->withPackage($package)
            ->withPayment($payment)
            ->withPaymethod($paymethod)
            ->withWeight($weight)
            ->withNow(date('D, d/M/Y'));
    }

    public function updatePackageRow($id)
    {
        $package = Package::withTrashed()->find($id);
        $response =
            $package
                ? View::make('components.packages.rows')->withPackage($package)
                : null;
        return $response;
    }

    public function edit($package_id)
    {
        $statuses = Status::orderBy('id');

        if( !Auth::user()->isOwner() )
            $statuses->where('id','<>', 9);

        $package = Package::withTrashed()->find($package_id);

        $view = View::make('packages.edit')
            ->withPackage($package)
            ->withStatuses($statuses->lists('name', 'id'));

        return $view;
    }

    public function printBill($package_id)
    {
        $package = Package::withTrashed()->find($package_id);
        if($package) {
            $pdf  = $this->getBill($package, 'components.packages.show');
            $status = Status::getStatusByName('Facturado')->first();
            $package->updateStatuses($status);
            $package->status_id = $status->id;
            $package->save();
            return $pdf->download($package->tracking.'.pdf');
        }
    }

    public function findPackage()
    {
        return Package::get(['id', 'tracking as name']);
    }

    public function findUsername($package_id)
    {
        return Package::findOrFail($package_id)->user->full_name;
    }
}

