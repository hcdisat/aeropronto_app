<?php

class ProfilesController extends \BaseController {


	/**
	 * Update the specified resource in storage.
	 * PUT /profiles/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$profile = Profile::find(Input::get('id'));
        if($profile) {
            Flash::success('Contacto actualizado');
            $profile->update(Input::all());
            return Redirect::back();
        }
        Flash::error('Error actualizando contacto.');
        return Redirect::back()->withInputs(Input::all());
	}


	/**
	 * user profile
	 */
	public function userProfile()
	{

		$data = [
			'addresses' => Auth::user()->addresses,
			'phones' => Auth::user()->phones,
			'cards' => Auth::user()->cards
		];

		return View::make('users.profile', $data);
	}
}