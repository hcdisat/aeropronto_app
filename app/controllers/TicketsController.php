<?php

use DisatCorp\Transformers\TicketsTransformer;
use Illuminate\Database\Eloquent\Collection;

class TicketsController extends \BaseController
{

	protected $transformer;
	protected $ticketForm;

	/**
	 * TicketsController constructor.
	 * @param TicketsTransformer $transformer
	 * @param \DisatCorp\Forms\TicketForm $form
	 */
	public function __construct(TicketsTransformer $transformer, \DisatCorp\Forms\TicketForm $form)
	{
		$this->transformer = $transformer;
		$this->ticketForm = $form;
	}


	/**
	 * Display a listing of the resource.
	 * GET /tickets
	 *
	 * @return Response
	 */
	public function index()
	{
		return $this->isAdminOrServiceUser()
			? Redirect::route('admin.tickets.index')
			: View::make('helpdesk.index');
	}

	public function getTickets()
	{
		$status = is_null(Input::get('status')) ? 1 : Input::get('status');

		$tickets = Auth::user()->clienTickets()->whereTicketStatusId($status)->get();

		return [
			'data' => $this->transformer->clientTransformCollection($tickets)
		];
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tickets/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$packages = Auth::user()
			->packages()->orderBy('tracking', 'desc')
			->lists('tracking', 'id');
		$subjects = TicketSubject::lists('subject', 'id');

		$view = View::make('helpdesk.create')
			->withPackages($packages)
			->withSubjects($subjects)
			->render();

		return [
			'data' => $view
		];
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tickets
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$wasCreatedByAdmins = $this->isAdminOrServiceUser();

		if( $wasCreatedByAdmins && !Input::has('package_id') )
		{
			$message = 'Por favor selecione un paquete';
			return [
				'data' => null,
				'errors' => [$message],
				'status' => 402
			];
		}

		$user_id = $wasCreatedByAdmins
			? Package::findOrFail(Input::get('package_id'))->user->id
			: Auth::user()->id;

		$subject = Input::get('subject_id') != 0
			? TicketSubject::findOrFail(Input::get('subject_id'))->subject
			: Input::get('subject');


		$data = array_merge([
			'user_id' => $user_id,
			'ticket_status_id' => 1,
			'ticket_priority_id' => 1
		], Input::except(['find-package', 'subject_id']));

		$data['subject'] = $subject;

		$this->ticketForm->validate($data);

		$ticket = Ticket::create($data);

		if( $wasCreatedByAdmins )
		{
			$ticket->is_asigned = true;
			$ticket->save();
			$ticket->users()->attach(Auth::user()->id);
		}

		return ['data' => $ticket, 'status' => 200];
	}

	/**
	 * Display the specified resource.
	 * GET /tickets/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data = Auth::user()->isAdmin() || Auth::user()->hasRole('service')
			? $this->getShowViewData()
			: [];

        extract($data);

		$ticket = Ticket::findOrFail($id);
		return View::make('helpdesk.show', compact('users', 'statuses', 'priorities'))
			->withTicket($ticket);
	}

	protected function getShowViewData()
	{
//		dd(User::clientService()->toArray());
		$users = $this->transformer->formatUserList(User::clientService());
		$statuses = TicketStatus::lists('name', 'id');
        $priorities = TicketPriority::lists('name', 'id');

        return [
            'users' => $users,
            'statuses' => $statuses,
            'priorities' => $priorities
        ];
	}

	/**
	 * @return bool
	 */
	private function isAdminOrServiceUser()
	{
		return (Auth::user()->hasRole('service') || Auth::user()->hasRole('admin'));
	}

}