<?php

use DisatCorp\Api\Reports\ReportableTrait;

class ReportsController extends \BaseController {
    use ReportableTrait;

    /**
     * Display a listing of the resource.
     * GET /reports
     *
     * @param $package_id
     * @return Response
     */
	public function printBill($package_id)
	{
        $pdf  = $this->getBill($package_id, 'components.packages.show');
		return $pdf->download('Factura.pdf');
	}

	/**
	 * All packages report
	 * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
	public function packageAll()
	{
		$view = 'reports.pdf.package';

		if( Request::isMethod('POST') )
		{
			if( !Input::has(['from', 'to']) )
			{
				Flash::error('Es necesario elegir un intervalo de fechas');
				return Redirect::back();
			}

			$from = \Carbon\Carbon::parse(Input::get('from'))->toDateString();
			$to = \Carbon\Carbon::parse(Input::get('to'))->toDateString();

			$data = $this->packageReport($from, $to, true);

			if( Input::get('type') == 'pdf' )
				return \PDF::loadView($view, $data)
					->download('packages_from_'.$from.'_to_'.$to.'.pdf');

		} else {
			$data = [
				'report_name' => 'Todos los paquetes',
			];
		}

		return View::make('reports.index', $data);
	}

	/**
	 * All billed packages
	 * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
	public function packageBilled()
	{
		$view = 'reports.pdf.package';

		if( Request::isMethod('POST') )
		{
			if( !Input::has(['from', 'to']) )
			{
				Flash::error('Es necesario elegir un intervalo de fechas');
				return Redirect::back();
			}

			$from = \Carbon\Carbon::parse(Input::get('from'))->toDateString();
			$to = \Carbon\Carbon::parse(Input::get('to'))->toDateString();

			$data = $this->packageReport($from, $to, false);

			if( Input::get('type') == 'pdf' )
				return \PDF::loadView($view, $data)
					->download('billed_packages_from_'.$from.'_to_'.$to.'.pdf');

		} else {
			$data = [
				'report_name' => 'Paquetes Facturados',
			];
		}

		return View::make('reports.index', $data);
	}

	/**
	 * Tickets Report
	 * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
	public function tickets()
	{
		$view = 'reports.pdf.ticket';
		$reportName = 'Reclamaciones no cerradas';

		if( Request::isMethod('POST') )
		{
			if( !Input::has(['from', 'to']) )
			{
				Flash::error('Es necesario elegir un intervalo de fechas');
				return Redirect::back();
			}

			$from = \Carbon\Carbon::parse(Input::get('from'))->toDateString();
			$to = \Carbon\Carbon::parse(Input::get('to'))->toDateString();


			$report = Ticket::where('ticket_status_id', '<>', 2)->get();

			$data = [
				'report_name' => $reportName,
				'report' => $report,
				'from' => $from,
				'to' => $to,
				'view' => $view
			];

			if( Input::get('type') == 'pdf' )
				return \PDF::loadView($view, $data)
					->download('billed_packages_from_'.$from.'_to_'.$to.'.pdf');

		} else {
			$data = [
				'report_name' => $reportName,
			];
		}

		return View::make('reports.index', $data);
	}

	protected function packageReport($from, $to, $all = true)
	{
		$view = 'reports.pdf.package';

		$report = $all
			? PackageAllReport::whereBetween('created_at', [$from, $to])
			: PackageReport::whereBetween('created_at', [$from, $to]);


		$data = [
			'report_name' => 'Todos los paquetes',
			'report' => $report->orderBy('created_at')->get(),
			'from' => $from,
			'to' => $to,
			'view' => $view
		];

		return $data;
	}
}