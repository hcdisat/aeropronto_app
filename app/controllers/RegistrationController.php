<?php

use \DisatCorp\Forms\RegistrationForm;

class RegistrationController extends \BaseController {

    function __construct(RegistrationForm $registrationForm)
    {
        $this->registrationForm = $registrationForm;
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /registration/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return Auth::guest()
            ? View::make('registration.create')
            : Redirect::route('pages.home');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /registration
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only('fname', 'lname', 'email', 'password', 'password_confirmation');

        $this->registrationForm->validate($inputs);

        $confirmation_code = str_random(30);
        $inputs = array_merge($inputs, ['confirmation_code' => $confirmation_code]);

		$user = User::create($inputs);
        $role = Role::whereName('user')->first();
        $user->roles()->attach($role);

        Flash::message(Configuration::getConfig('mensage_registro')->value);

        Mail::send('emails.notifications.verify', ['confirmation_code' => $confirmation_code],
            function ($message) use ($user){
                $message->to($user->email, $user->getFullName())
                    ->subject('Confirme su correo.');
        });

        return Redirect::route('pages.home');
	}

    public function confirm($confirmation_code)
    {
        $errorMessage = 'Código de Confirmación inválido';
        $successMessage = 'Su cuenta fue validada con exito.';

        if( !$confirmation_code){
            Flash::error($errorMessage);
            Return Redirect::route('pages.home');
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if( !$user) {
            Flash::error($errorMessage);
            Return Redirect::route('pages.home');
        }

        $user->confirmation_code = null;
        $user->confirmed = true;
        $user->save();

        Flash::success($successMessage);
        Return Redirect::route('pages.home');
    }

}