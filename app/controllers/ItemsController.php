<?php

use DisatCorp\Commands\AddItemCommand;
use DisatCorp\Forms\ItemForm;

class ItemsController extends \BaseController
{

    use \Laracasts\Commander\CommanderTrait;

    protected $itemForm;

    function __construct(ItemForm $itemForm)
    {
        $this->itemForm = $itemForm;
    }

    public function edit($id)
    {
        $item = Item::find($id);

        return $item
            ? $this->response($item, false, 'Success')
            : $this->response(null, true, 'Item Not Found :(');
    }

    public function destroy($id)
    {
        $item = Item::find($id);
        return $item->delete()
            ? $this->response($item, null, 'Success')
            : $this->response(null,   true, 'Fail');
    }

    public function update($id)
    {
        try
        {
            $pound = Configuration::getPricePerPound()->value;

            $this->itemForm->validate(
                Input::only('description', 'package_id', 'weight', 'name'));
            $price = ($pound * Input::get('weight'));
            $item = Item::find($id);

            $item->update(array_merge(Input::all(), ['price' => $price]));
            $price = ($pound * Input::get('weight'));
            $item = Item::find($id);

            $item->update(array_merge(Input::all(), ['price' => $price]));
            return View::make('components.items.items-rows')->withItem($item);
        }
        catch(\Laracasts\Validation\FormValidationException $ex)
        {
            return $this->response(null, true, $ex->getErrors());
        }

    }

    public function store()
    {
        try
        {
            $this->itemForm->validate(
                Input::only('description', 'package_id', 'weight', 'name'));

            $item = $this->execute(AddItemCommand::class);
            return $this->response($item, false, 'Success');

        } catch (\Laracasts\Validation\FormValidationException $ex)
        {
            return $this->response(null, true, $ex->getErrors());
        }
    }

    public function getItemsTable($package_id) {

        return  View::make('components.items.items-table')
            ->withItems(Item::wherePackageId($package_id)->get());
    }

    private function response($data, $error, $message)
    {
        return [
            'data' => $data,
            'error' => $error,
            'message' => $message
        ];
    }
}