<?php

class PagesController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /pages
     *
     * @return Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return View::make('pages.index')
            ->withSliders($sliders);
    }

    public function show()
    {
        //Flash::message('Oh Oh! An error occurred :(');

        $pdf = PDF::loadView('components.reports.factura');
        return $pdf->stream('foo.pdf');
    }

    public function getPackagesStatus()
    {
        $packages = Package::with('statuses')
            ->whereTracking(Input::get('search'))
            ->get()
            ->first();

        return ($packages and $packages->user_id == Auth::user()->id)
            ? View::make('pages.users.index')->withPackage($packages)->withStatus($packages->status)
            : View::make('pages.users.index')->withError('No existen paquetes con ese código');
    }

    // Pages Actions
    public function about()
    {
        return View::make('pages.about')
            ->withWe(Configuration::getConfig('we'))
            ->withMission(Configuration::getConfig('mission'))
            ->withVision(Configuration::getConfig('vision'))
            ->withValues(Configuration::getConfig('values'));
    }

    public function contact()
    {
        $profile = Profile::first();
        return View::make('pages.contact')->withProfile($profile);
    }

    public function error()
    {
        return View::make('pages.error');
    }
}