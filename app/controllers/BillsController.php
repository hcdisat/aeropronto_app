<?php


use Laracasts\Commander\CommanderTrait;

class BillsController extends \BaseController
{
	use CommanderTrait;
	/**
	 * @var \DisatCorp\Helpers\CreditCardsHelper
	 */
	protected $helper;
	protected $cardForm;

	/**
	 * BillsController constructor.
	 * @param \DisatCorp\Helpers\CreditCardsHelper $helper
	 */
	public function __construct(\DisatCorp\Helpers\CreditCardsHelper $helper, \DisatCorp\Forms\CardForm $cardForm)
	{
		$this->helper = $helper;
		$this->cardForm = $cardForm;
	}


	/**
	 * Display a listing of the resource.
	 * GET /bills
	 *
	 * @return Response
	 */
	public function index()
	{
		$packages = Auth::user()->packages;
		return View::make('bills.index')
			->withPackages($packages);
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /bills/create
	 *
	 * @param $package_id
	 * @return Response
	 */
	public function payment($package_id)
	{
		$package = Package::findOrFail($package_id);

		if( $package->hasStatus(7) )
		{
			Flash::error('El paquete ya fue pagado. si cree que esto es un error comuniquese con nosotros.');
			return Redirect::route('pages.packages');
		}

		if( !$package->hasItems() )
		{
			Flash::error('Paquete en proceso. No esta disponible aun para procesar pagos.');
			return Redirect::route('pages.packages');
		}

		$taxes = ($package->items->sum('price') * Configuration::getConfig('taxes')->value);
//		$total = $package->items->sum('price') + $taxes;

		$bill = [
			'package_id' => $package->id,
			'taxes' => $taxes,
			'total' => $package->items->sum('price'),
			'weight' => $package->items->sum('weight'),
		];

		Session::put('bill', $bill);

		return View::make('bills.payment')
			->withPackage($package);
	}

	/**
	 * @return mixed
	 */
	public function setPaymentMethod()
	{
		$cards = Auth::user()->cards->lists('last_four', 'id');

		return View::make('bills.payment-mehod')->withCards($cards);
	}

	/**
	 *
	 */
	public function pay()
	{
		$pay = function()
		{
			$taxes = Session::get('bill.taxes');
			$payment = Session::get('bill.total');
			$weight = Session::get('bill.weight');
			$package_id = Session::get('bill.package_id');

			$bill = $this->execute(new \DisatCorp\Commands\NewBillCommand(
				$package_id,
				$taxes,
				$payment,
				$weight,
				true
			));

			return $bill;
		};

		$bill = null;
		if( Input::has('last_four') )
		{
			$this->payWithNewCard(Input::all());
			$bill = $pay();
		}

		if( Input::has('cardNumber') )
		{
			$cardData = Card::findOrFail(Input::get('cardNumber'));
			$card = $this->helper->validate($cardData->code);
			$this->validateCard($card, $cardData->expiration_year, $cardData->expiration_month);
			$bill = $pay();
		}

		if( !is_null($bill) )
		{
			$package = $bill->package;
			$now = date('D, d/M/Y');
			$payment = Session::get('bill.total');
			$weight = Session::get('bill.weight');
			$paymethod = 'Online';
			$data = compact('package', 'payment', 'weight', 'now', 'paymethod');
			$view = \PDF::loadView('components.packages.show', $data);

			return $view->download($bill->code.'.pdf');
		}

		Flash::error('Selecione una forma de pago.');
		return Redirect::route('pages.packages');
	}


	/**
	 * @param $data
	 * @return bool
	 */
	private function validateNewCard($data)
	{

		try
		{
			$this->cardForm->validate((array) $data);
		}
		catch(\Laracasts\Validation\FormValidationException $e)
		{
			Flash::errors($e->getErrors());
			$result = [
				'valid' => false,
				'message' => 'La tarjeta no es valida.'
			];
			return (object) $result;
		}

		$card = $this->helper->validate($data->last_four);

		return $this->ValidateCard($card, $data->expiration_year, $data->expiration_month);
	}

	/**
	 * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
	private function payWithNewCard()
	{
		$data = (object)Input::all();

		unset($data->_token);
		unset($data->cardBumber);
		unset($data->_token);
		$status = $this->validateNewCard($data);

		if ($status->valid)
			Flash::success('El pago fue realizado exitosamente.');
		else
			Flash::error('La tarjeta no es valida');
	}

	/**
	 * @param $card
	 * @param $expirationYear
	 * @param $expirationMonth
	 * @return object
	 * @internal param $data
	 */
	private function ValidateCard($card, $expirationYear, $expirationMonth)
	{
		$result = [
			'valid' => true,
			'message' => 'El pago fue realizado exitosamente.'
		];

		if (!$card->valid)
			$result = [
				'valid' => false,
				'message' => 'El número de la tarjeta no es válido.'
			];

		if (!$this->helper->validExpirationDate('20' . $expirationYear, $expirationMonth))
			$result = [
				'valid' => false,
				'message' => 'La fecha de expiracion no es válida.'
			];

		return (object) $result;
	}


	public function execute($command, array $input = null, $decorators = [])
	{
		$bus = $this->getCommandBus();

		return $bus->execute($command);
	}
}