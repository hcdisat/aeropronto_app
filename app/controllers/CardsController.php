<?php

class CardsController extends \BaseController
{

	/**
	 * @var \DisatCorp\Forms\CardForm
	 */
	protected $form;
	protected $helper;

	/**
	 * CardsController constructor.
	 * @param \DisatCorp\Forms\CardForm $form
	 * @param \DisatCorp\Helpers\CreditCardsHelper $helper
	 */
	public function __construct(\DisatCorp\Forms\CardForm $form, \DisatCorp\Helpers\CreditCardsHelper $helper)
	{
		$this->form = $form;
		$this->helper = $helper;
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /cards/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /cards
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = $this->parseCardData();

		Card::create($data);

		Flash::success('Tarjeta agregada a su lista');
		return Redirect::route('users.profile');

	}


	/**
	 * Show the form for editing the specified resource.
	 * GET /cards/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$card = Card::findOrFail($id);

		return View::make('users.create')
			->withCard($card);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /cards/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$creditCard = Card::findOrFail($id);

		$data = $this->parseCardData();

		$creditCard->update($data);
		Card::finalizeCard($creditCard);
		Flash::success('Tarjeta editada correctamente.');
		return Redirect::route('users.profile');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /cards/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$card = Card::findOrFail($id);
		$card->delete();

		Flash::success('la latjeta fue eliminada de su lista.');
		return Redirect::route('users.profile');
	}

	/**
	 * @param null $code
	 * @return \Illuminate\Http\JsonResponse|null
	 */
	public function getCardType($code = null)
	{

		return !is_null($code)
			? Response::json(['data' => $this->helper->getCardType($code)], 200)
			:	null;
	}

	/**
	 * @return object
	 * @throws \Laracasts\Validation\FormValidationException
	 */
	private function validateCard()
	{
		$this->form->validate(Input::all());

		$cardCode = Input::get('last_four');
		$card = $this->helper->validate($cardCode);
		return $card;
	}

	/**
	 * @return array
	 */
	private function parseCardData()
	{
		$card = $this->validateCard();

		$card->last_four = $card->number;

		if( !$card->valid )
		{
			Flash::error('Tarjeta no valida.');
			return Redirect::back()
				->withInput(Input::all());
		}

		unset($card->valid);
		unset($card->number);
		$data = array_merge(Input::all(), (array)$card);
		return $data;
	}

}