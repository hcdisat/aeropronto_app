<?php 

use \DisatCorp\Forms\ImagesForm;

class SliderController extends \BaseController
{
    const INDEX_ROUTE= 'settings.slider.index';

    const INDEX_VIEW ='configurations.slider.index';
    const EDIT_VIEW = 'configurations.slider.edit';
    const CREATE_VIEW ='configurations.slider.create';

    protected $validator;

    function __construct(ImagesForm $validator)
    {
        $this->validator = $validator;
    }


    /**
     * Display a listing of the resource.
     * GET /slider
     *
     * @return Response
     */
    public function index()
    {
        return View::make(self::INDEX_VIEW)
            ->withSliders(Slider::paginate(3));
        //->withDeletedSliders(Slider::onlyTrashed()->paginate(3));
    }

    /**
     * Show the form for creating a new resource.
     * GET /slider/create
     *
     * @return Response
     */
    public function create()
    {
        return View::make(self::CREATE_VIEW);
    }

    /**
     * Store a newly created resource in storage.
     * POST /slider
     *
     * @return Response
     */
    public function store()
    {
        $this->validator->validate(Input::all());
        $file = Input::file('image');

        if($file and $file->isValid()){
            $name = $this->saveImage(null, $file);
            Slider::create((array_merge(Input::all(), ['image' => $name])));

            Flash::success('Slider creado correctamente.');
            return Redirect::route(self::INDEX_ROUTE);
        }
        Flash::success('ERROR creando el slider. Intente más tarde');
        return Redirect::back();
    }


    /**
     * Show the form for editing the specified resource.
     * GET /slider/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        if($slider){
            return View::make(self::EDIT_VIEW)->withSlider($slider);
        }
        Flash::error('No se encontró el slider Especificado');
        return Redirect::route(self::INDEX_ROUTE);

    }

    /**
     * Update the specified resource in storage.
     * PUT /slider/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $slider = Slider::find($id);
        if( !$slider) {
            Flash::error('Error subiendo la Imagen, corrija los errores e intente de nuevo');
            return Redirect::back()->withInputs(Input::all());
        }

        $data = Input::all();
        $data['image'] = $data['image_old'];
        unset($data['image_old']);

        $this->validator->validate($data);
        $file = Input::file('image_update');

        if($file and $file->isValid()){
            $name = $this->saveImage($slider, $file);

            $slider->update(array_merge($data, ['image' => $name]));

            Flash::success('Imagen subida al servidor');
            return Redirect::route(self::INDEX_ROUTE);
        }
        $slider->update(['caption' => $data['caption']]);
        Flash::success('Slider Actualizado.');
        return Redirect::route(self::INDEX_ROUTE);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /slider/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        $slider = Slider::find($id);
        if($slider){
            $slider->delete();
            Flash::success('Slider Eliminado.<br/>Puede restaurarlo nuevamente desde la sección "Restaurar"');
        } else {
            Flash::error('No se puede Eliminar el Slider ahora. Intente más tarde');
        }
        return Redirect::back();
    }

    public function restore()
    {
        return View::make(self::INDEX_VIEW)
            ->withSliders(Slider::onlyTrashed()->paginate(3));
    }

    public function undelete($id)
    {
        $slider = Slider::onlyTrashed()->whereId($id)->first();
        if($slider and $slider->deleted_at) {
            $slider->deleted_at = null;
            $slider->save();
            Flash::success('El "Slider" fué restaurado.');
        } else {
            Flash::success('No es posible restaurar el "Slider".');
        }
        return Redirect::back();
    }

    /**
     * @param Slider $slider
     * @param $file
     * @internal param $id
     * @return string
     */
    private function saveImage(Slider $slider = null, $file)
    {
        $uploadPath = Configuration::getConfig('uploads_path')->value;
        $name = !$slider
            ? str_random(10) . '.' . $file->getClientOriginalExtension()
            : $slider->image() ;

        $file->move($uploadPath, $name);
        return $name;
    }

}