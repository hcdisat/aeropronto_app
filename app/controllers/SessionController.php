<?php
use DisatCorp\Forms\LoginForm;

class SessionController extends \BaseController {

    function __construct(LoginForm $loginForm)
    {
        $this->loginForm = $loginForm;
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /session/create
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('sessions.login');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /session
	 *
	 * @return Response
	 */
	public function store()
	{
        $this->loginForm->validate($inputs = Input::only('email', 'password'));

		$intended = Session::has('intended') ? Session::pull('intended') : '/';
        return Auth::attempt(array_merge($inputs, ['confirmed' => 1]))
            ? Redirect::intended($intended)
            : Redirect::back()
                ->withInput()
                ->withFlashMessage('Usuario ó Password Incorrectos');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /session/
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
        Return Redirect::route('pages.home');
	}

}