<?php

class PhonesController extends \BaseController
{

	/**
	 * @var \DisatCorp\Forms\PhoneForm
	 */
	protected $form;

	/**
	 * PhonesController constructor.
	 * @param \DisatCorp\Forms\PhoneForm $form
	 */
	public function __construct(\DisatCorp\Forms\PhoneForm $form)
	{
		$this->form = $form;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /phones/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$types = PhoneType::all()->lists('name', 'id');
		return View::make('users.create')
			->withTypes($types);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /phones
	 *
	 * @return Response
	 */
	public function store()
	{
		$this->form->validate(Input::all());

		Phone::create(Input::all());

		Flash::success('El teléfono se agregó a su lista');

		return Redirect::route('users.profile');
	}


	/**
	 * Show the form for editing the specified resource.
	 * GET /phones/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$phone = Phone::findOrFail($id);

		$types = PhoneType::all()->lists('name', 'id');

		return View::make('users.create')
			->withTypes($types)
			->withPhone($phone);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /phones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$this->form->validate(Input::all());
		$phone = Phone::findOrFail($id);
		$phone->update(Input::all());

		Flash::success('Teléfono editado correctamente.');
		return Redirect::route('users.profile');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /phones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$phone = Phone::findOrFail($id);

		$phone->delete();

		Flash::success('El teléfono se ese eliminó de su lista.');

		return Redirect::route('users.profile');
	}

}