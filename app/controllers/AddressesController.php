<?php

class AddressesController extends \BaseController
{

	protected $form;


	/**
	 * AddressesController constructor.
	 * @param \DisatCorp\Forms\AddressForm $form
	 */
	public function __construct(\DisatCorp\Forms\AddressForm $form)
	{
		$this->form = $form;
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /addresses/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$cities = City::all()->lists('name', 'id');
		return View::make('users.create')
			->withCities($cities);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /addresses
	 *
	 * @return Response
	 */
	public function store()
	{
		$this->form->validate(Input::all());
		Address::create(Input::all());

		Flash::success('La dirección fue creada');
		return Redirect::route('users.profile');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /addresses/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$address = Address::findOrFail($id);
		$cities = City::all()->lists('name', 'id');
		return View::make('users.create')
			->withAddress($address)
			->withCities($cities);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /addresses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$this->form->validate($data = Input::all());

		$address = Address::findOrFail($id);

		$address->update($data);

		Flash::success('La dirección fue editada');
		return Redirect::route('users.profile');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /addresses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$address = Address::findOrFail($id);

		$address->delete();

		Flash::success('La dirección fue eliminada de su lista.');
		return Redirect::route('users.profile');
	}

}