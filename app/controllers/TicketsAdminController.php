<?php

class TicketsAdminController extends TicketsController
{

	/**
	 * Display a listing of the resource.
	 * GET /ticketsadmin
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('helpdesk.admin.index');
	}

    public function getTickets()
    {

        $tickets = Ticket::where('ticket_status_id', '<>', 2);

        $tickets = !Input::get('mine') ? $tickets
            : Auth::user()->asignedTickets();

        $tickets = !Input::has('asigned')
            ? $tickets
            : $tickets->whereIsAsigned(true);

        return [
            'data' => $this->transformer->clientTransformCollection($tickets->get())
        ];
    }

    public function update($ticket_id)
    {
        $userIds = Input::only('user_ids');

        // find the ticket
        $ticket = Ticket::find($ticket_id);

        // if is null redirect back
        if( is_null($ticket) )
        {
            \DisatCorp\Notifications\Flash\Flash::error('El ticket no Existe');
            return Redirect::back();
        }

        // if not validate the data
        $this->ticketForm->setEditRules();
        $this->ticketForm->validate(Input::except('user_ids'));

        // update the model
        $ticket->update(array_merge([ 'is_asigned' => !empty($userIds['user_ids'])], Input::only([
            'ticket_status_id',
            'ticket_priority_id'
        ])));

        if( !empty($userIds['user_ids']))
            $ticket->users()->sync($userIds['user_ids']);

        // redirect back to the show view
        return Redirect::back();
    }


}