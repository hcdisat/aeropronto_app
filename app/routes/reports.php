<?php


Route::group(['prefix' => 'admin/reports', 'before' => 'roles:admin:owner'], function(){

    /*
     * Get
     */
    Route::get('packages/all', [
        'as' => 'packages-all',
        'uses' => 'ReportsController@packageAll'
    ]);

    Route::get('packages/billed', [
        'as' => 'packages-billed',
        'uses' => 'ReportsController@packageBilled'
    ]);

    Route::get('tickets', [
        'as' => 'tickets',
        'uses' => 'ReportsController@tickets'
    ]);

    /*
     * Post
     */
    Route::post('packages/all', [
        'as' => 'packages-all',
        'uses' => 'ReportsController@packageAll'
    ]);

    Route::post('packages/billed', [
        'as' => 'packages-billed',
        'uses' => 'ReportsController@packageBilled'
    ]);

    Route::post('tickets', [
        'as' => 'tickets',
        'uses' => 'ReportsController@tickets'
    ]);

});