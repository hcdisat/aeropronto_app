<?php

Route::group(['before' => 'role:admin'], function(){

//    Route::get('/find', [
//        'as' => 'user.find',
//        'uses' => 'UserController@findByName'
//    ]);

    Route::resource('user', 'UserController',
        ['only' => [
            'index', 'store', 'update',
            'edit', 'destroy'
        ]]);
});

Route::group(['before' => 'role:owner'], function(){

    Route::resource('user', 'UserController',
        ['only' => [
            'index', 'store', 'update',
            'edit', 'destroy'
        ]]);
});

Route::group(['before' => 'roles:admin:service'], function(){

    Route::get('/find', [
        'as' => 'user.find',
        'uses' => 'UserController@findByName'
    ]);
});