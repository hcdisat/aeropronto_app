<?php

Route::group(['before' => 'role:user'], function(){

    Route::get('tickets/get', [
        'as' => 'tickets.get',
        'uses' => 'TicketsController@getTickets'
    ]);

    Route::resource('tickets', 'TicketsController', [
        'only' => [
            'index',
            'show',
            'create',
            'store'
        ]
    ]);

    Route::post('comments', [
        'as' => 'comments',
        'uses' => 'CommentsController@store'
    ]);

});


Route::group(['prefix' => 'admin', 'before' => 'roles:admin:service'], function(){

    Route::get('tickets/get', [
        'as' => 'admin.tickets.get',
        'uses' => 'TicketsAdminController@getTickets'
    ]);

    Route::resource('tickets', 'TicketsAdminController', [
        'only' => [
            'index',
            'create',
            'show',
            'update'
        ]
    ]);
});