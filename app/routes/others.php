<?php
use Faker\Factory as Faker;

    Route::get('/testviews', 'PagesController@show');


Route::get('/cards', function() {

    $faker = Faker::create();
    $cardsInfo = $faker->creditCardDetails();
    return $cardsInfo;
});

Route::get('/test', function() {

    $years = Config::get('profile.years');
    return $years;
});