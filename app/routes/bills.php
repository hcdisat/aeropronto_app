<?php


Route::group(['before' => 'role:user'], function(){
    Route::get('users/packages', [
        'as' => 'pages.packages',
        'uses' => 'BillsController@index'
    ]);

    Route::get('users/packages/payment/{packageId}', [
        'as' => 'bill.payment',
        'uses' => 'BillsController@payment'
    ]);

    Route::post('/users/packages/payment', [
        'as' => 'bill.payment',
        'uses' => 'BillsController@setPaymentMethod'
    ]);

    Route::post('/users/packages/pay', [
        'as' => 'bill.payonline',
        'uses' => 'BillsController@pay'
    ]);
});