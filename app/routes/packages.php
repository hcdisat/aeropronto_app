<?php

Route::group(['before' => 'role:admin'], function(){
    Route::resource('package', 'PackageController', [
        'only' => [
            'index', 'create', 'store',
            'destroy', 'update', 'show', 'edit'
        ]
    ]);

    Route::get('package/row/{packageId}', [
        'as' => 'package.row',
        'uses' => 'PackageController@updatePackageRow'
    ]);

    Route::get('package/status/{packageId}', [
        'as' => 'package.status',
        'uses' => 'PackageController@getStatusPartial'
    ]);

    Route::get('package/bill/{package_id}', [
        'as' => 'reports.bill',
        'uses' => 'PackageController@printBill'
    ]);

    Route::post('/auth/validate', function(){

        if( Auth::attempt(Input::only(['email', 'password'])) )
            return Redirect::route('package.edit', Input::get('package_id'));

        Flash::error('Credenciales no validas');
        return Redirect::back();
    });

});

Route::get('packages/find/', 'PackageController@findPackage')
    ->before('roles:admin:service');

Route::get('packages/get/username/{package_id}', 'PackageController@findUsername')
    ->before('roles:admin:service');