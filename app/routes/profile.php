<?php


Route::group(['prefix' => 'users/profile', 'before' => 'role:user'], function(){

    /**
     * Index
     */

    Route::get('', [
        'as' => 'users.profile',
        'uses' => 'ProfilesController@userProfile'
    ]);


    /**
     * Address
     */
    Route::resource('address', 'AddressesController', [
        'only' => ['create', 'store', 'edit', 'update', 'destroy']
    ]);

    /**
     * Phones
     */
    Route::resource('phone','PhonesController', [
        'only' => ['create', 'store', 'edit', 'update', 'destroy']
    ]);


    /**
     * Credit Cards
     */
    Route::resource('card', 'CardsController', [
        'only' => ['create', 'store', 'edit', 'update', 'destroy']
    ]);

    Route::get('card/get-type/{code?}', [
        'as' => 'card.get-type',
        'uses' => 'CardsController@getCardType'
    ]);
});