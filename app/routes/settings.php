<?php

    Route::group(['before' => 'role:owner'], function(){
        Route::get('/settings', [
            'as' => 'settings.index',
            'uses' => 'ConfigurationsController@index'
        ]);

        Route::post('/settings', [
            'as' => 'settings.update',
            'uses' => 'ConfigurationsController@update'
        ]);
        Route::post('/settings/mission', [
            'as' => 'settings.mission',
            'uses' => 'ConfigurationsController@mission'
        ]);
        Route::post('/settings/we', [
            'as' => 'settings.we',
            'uses' => 'ConfigurationsController@whoWeAre'
        ]);
        Route::post('/settings/vision', [
            'as' => 'settings.vision',
            'uses' => 'ConfigurationsController@vision'
        ]);
        Route::post('/settings/values', [
            'as' => 'settings.values',
            'uses' => 'ConfigurationsController@values'
        ]);

        Route::post('/settings/contact/{id}', [
            'as' => 'settings.contact',
            'uses' => 'ProfilesController@update'
        ]);



        Route::get('/settings/subjects', [
            'as' => 'settings.subjects-add',
            'uses' => 'ConfigurationsController@addSubject'
        ]);
        Route::post('/settings/subjects', [
            'as' => 'settings.subjects-store',
            'uses' => 'ConfigurationsController@storeSubject'
        ]);
        Route::get('/settings/subjects/{id}/edit', [
            'as' => 'settings.subjects-edit',
            'uses' => 'ConfigurationsController@editSubject'
        ]);
        Route::put('/settings/subjects/{id}', [
            'as' => 'settings.subjects-update',
            'uses' => 'ConfigurationsController@updateSubject'
        ]);
        Route::delete('/settings/subjects/{id}', [
            'as' => 'settings.subjects-delete',
            'uses' => 'ConfigurationsController@deleteSubject'
        ]);


        Route::resource('settings/slider', 'SliderController', [
            'only' => [
                'index', 'edit', 'update',
                'create', 'store'
            ]
        ]);
        Route::get('settings/slider/delete/{id}', [
            'as' => 'settings.slider.delete',
            'uses' => 'SliderController@delete'
        ]);

        Route::get('settings/slider/reactivate/', [
            'as' => 'settings.slider.reactivate',
            'uses' => 'SliderController@restore'
        ]);
        Route::get('settings/slider/undelete/{id}', [
            'as' => 'settings.slider.undelete',
            'uses' => 'SliderController@undelete'
        ]);

    });

    Route::get('/showalerts/{level}/{message}', [
        'as' => 'settings.showalerts',
        'uses' => 'ConfigurationsController@showAlerts'
    ]);

    Route::get('/showalerts/{level}/{message}', [
        'as' => 'settings.showalerts',
        'uses' => 'ConfigurationsController@showAlerts'
    ]);