<?php

    Route::get('login', [
        'as' => 'session.login',
        'uses' => 'SessionController@create'
    ]);
    Route::get('logout', [
        'as' => 'session.logout',
        'uses' => 'SessionController@destroy@destroy'
    ]);

    Route::resource('session', 'SessionController', [
        'only' => [
            'create',
            'store',
            'destroy'
        ]
    ]);

/**
 * Registration
*/
    Route::get('/register', 'RegistrationController@create');
    Route::post('/register', [
        'as' => 'registration.store',
        'uses' => 'RegistrationController@store'
    ]);

    Route::get('register/verify/{confirmation_code}', [
        'as' => 'registration.confirmation',
        'uses' => 'RegistrationController@confirm'
    ]);