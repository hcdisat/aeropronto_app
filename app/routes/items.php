<?php

    Route::resource('items', 'ItemsController', [
        'only' => ['store', 'edit', 'destroy', 'update']
    ]);

    Route::get('/items/table/{package_id}',
        'ItemsController@getItemsTable');