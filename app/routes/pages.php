<?php

    Route::get('/', [
        'as' => 'pages.home',
        'uses' => 'PagesController@index'
    ]);

    Route::get('about', [
        'as' => 'pages.about',
        'uses' => 'PagesController@about'
    ]);

    Route::get('contact', [
        'as' => 'pages.contact',
        'uses' => 'PagesController@contact'
    ]);

    Route::get('error', [
        'as' => 'pages.error',
        'uses' => 'PagesController@error'
    ]);