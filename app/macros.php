<?php

    HTML::macro('payment', function($quantity)
    {
        return $quantity
            ? number_format($quantity, 2, '.', ',')
            : '0.00';
    });

    HTML::macro('totalWeight', function($weight){
        return $weight
            ? number_format($weight, 2, '.', ',')
            : 'N/A';
    });

    HTML::macro('parseDate', function($date){
        return \Carbon\Carbon::parse($date)
            ->subDay()->format('d/m/Y');
    });

    HTML::macro('img', function($image){

        return HTML::image(Configuration::getConfig('uploads_path').$image, $image,
            ['class' => 'img-responsive']);
    });

    HTML::macro('slider', function(){

        $counter = 0;
        $view = "<div class='%s item'>".
	                HTML::image('%s', '%s', ['class' => 'img-responsive']).
	                "<div class='carousel-caption'>
		                <p>%s</p>
	                </div>
                </div>";
        $sld = '';

        foreach(Slider::all() as $slider) {
            $sld .= $counter++ == 0
                ? sprintf($view, 'active',
                    Configuration::getConfig('uploads_path')->value.$slider->image, $slider->image, $slider->caption)
                : sprintf($view, '',
                    Configuration::getConfig('uploads_path')->value.$slider->image, $slider->image, $slider->caption);
        }
        return $sld;
    });

HTML::macro('labelStatus', function(TicketStatus $status){

    $ction = function($class) use ($status){
        $markup = '<span class="label label-%s">%s</span>';
        return sprintf($markup, $class, $status->name);
    };

    switch($status->id){
        case 1:
            return $ction('info');
            break;
        case 2:
            return $ction('danger');
            break;
        case 3:
            return $ction('default');
            break;
    }
});

HTML::macro('labelPrioriry', function(TicketPriority $priority){

    $ction = function($class) use ($priority){
        $markup = '<span class="label label-%s">%s</span>';
        return sprintf($markup, $class, $priority->name);
    };

    switch($priority->id){
        case 1:
            return $ction('warning');
            break;
        case 2:
            return $ction('primary');
            break;
        case 3:
            return $ction('danger');
            break;
    }
});

HTML::macro('labelYesOrNo', function($condition){

    $markup = '<span class="label label-pill label-%s">%s</span>';
    $class = 'default';
    $value = 'No';

    if( $condition ){
        $class = 'primary';
        $value = 'Si';
    }

    return sprintf($markup, $class, $value);

});

HTML::macro('showComment', function(TicketComment $comment){

    $pic = '55C1E7';
    $handed = 'left';

    if( ($comment->user->inRoles(['admin', 'service', 'owner'])) )
    {
        $handed = 'right';
        $pic = 'FA6F57';

    }

    $view = View::make('helpdesk.comments.display-comment', ['comment' => $comment]);
    $markup = $view->render();

    return str_replace([':handed', ':pic'], [$handed, $pic], $markup);
});