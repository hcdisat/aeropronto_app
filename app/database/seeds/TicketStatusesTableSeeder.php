
<?php


class TicketStatusesTableSeeder extends Seeder {

	public function run()
	{
		$statuses = [
			[
				'name' => 'Abierto',
				'description' => 'Ticket en estado abierto.'
			],
			[
				'name' => 'Cerrado',
				'description' => 'Ticket cerrado, el ticket fue atendido.'
			],
			[
				'name' => 'Pausado',
				'description' => 'Ticket pausado. El ticket se reabrira cuando acaben las investigaciones.'
			]
		];

		foreach($statuses as $status)
		{
			TicketStatus::create($status);
		}
	}

}