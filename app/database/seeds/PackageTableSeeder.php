<?php


class PackageTableSeeder extends Seeder {

	public function run()
	{

		foreach(range(1, 10) as $index)
		{
			$package = Package::create([
				'user_id' => 4,
				'status_id' => 1,
			]);

			$package->tracking = 'P-SDQ-0'.$package->id;
			$package->save();
		}
	}

}