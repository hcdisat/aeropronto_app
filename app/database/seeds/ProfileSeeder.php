<?php

class ProfileSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// profile module
		$this->call(CityTableSeeder::class);
		$this->call(PhoneTypeTableSeeder::class);

		$this->call(AddressTableSeeder::class);
		$this->call(PhoneTableSeeder::class);
	}

}
