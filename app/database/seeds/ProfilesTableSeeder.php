<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfilesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

        Profile::create([
            'address' => 'Some Street #66, Sector, City, Country',
            'user_id' => 1,
            'email' => 'hcdisat@gmail.com',
            'phone_one' => '1000-000-0000',
            'phone_two' => '1000-000-0000',
            'city' =>  'Santo Domingo DN'
        ]);
	}

}