<?php

// Composer: "fzaninotto/faker": "v1.3.0"
    use Faker\Factory as Faker;

    class StatusTableSeeder extends Seeder {

        public function run()
        {
            $faker = Faker::create();
            $cntr = 1;
            Status::create([
                'name' => 'Recibido',
                'description' => 'Paquete recibido en Almacén Miami',
                'abbr' => 'PR',
                'css_class' => 'primary',
                'order' => $cntr++
            ]);
            Status::create([
                'name' => 'Embarcado',
                'description' => 'Paquete Embarcado.',
                'abbr' => 'PE',
                'css_class' => 'green',
                'order' => $cntr++
            ]);
            Status::create([
                'name' => 'En Aduanas',
                'description' => 'Paquete recibido en AILA',
                'abbr' => 'AILA',
                'css_class' => 'yellow',
                'order' => $cntr++
            ]);
            Status::create([
                'name' => 'En Tránsito - Sucursal',
                'description' => 'Paquete en transito hacia sucursal',
                'abbr' => 'PS',
                'css_class' => 'red',
                'order' => $cntr++
            ]);
            Status::create([
                'name' => 'Recibido en Sucursal',
                'description' => 'Paquete recibido en sucursal',
                'abbr' => 'RS',
                'css_class' => 'success',
                'order' => $cntr++
            ]);
            Status::create([
                'name' => 'Disponible',
                'description' => 'Paquete disponible para retirar',
                'abbr' => 'PD',
                'css_class' => 'warning',
                'order' => $cntr++
            ]);
            Status::create([
                'name' => 'Facturado',
                'description' => 'Paquete Facturado',
                'abbr' => 'PF',
                'css_class' => 'danger',
                'order' => $cntr++
            ]);
            Status::create([
                'name' => 'Despachado',
                'description' => 'Paquete despachado',
                'abbr' => 'PEP',
                'css_class' => 'default',
                'order' => $cntr++
            ]);

            Status::create([
                'name' => 'Cancelado',
                'description' => 'El paquete fue cancelado',
                'abbr' => 'PC',
                'css_class' => 'calceled',
                'order' => $cntr
            ]);
        }
    }