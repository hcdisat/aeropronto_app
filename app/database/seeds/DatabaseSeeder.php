<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('StatusTableSeeder');
        $this->call('ConfigurationTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('ProfilesTableSeeder');
        $this->call('SlidersTableSeeder');

		/*
		 * Helpdesk
		 */

		$this->call(TicketPrioritiesTableSeeder::class);
		$this->call(TicketStatusesTableSeeder::class);
//		$this->call(TicketsTableSeeder::class);


		/*
		 * Packages TODO: COMMENT THIS PART
		 */
		//$this->call(PackageTableSeeder::class);

		// profile module
		$this->call(CityTableSeeder::class);
		$this->call(PhoneTypeTableSeeder::class);

		$this->call(AddressTableSeeder::class);
		$this->call(PhoneTableSeeder::class);
	}

}
