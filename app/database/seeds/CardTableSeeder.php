<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CardTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			$card = $faker->creditCardDetails();
			$expiration = explode('/', $card['expirationDate']);
			$name = explode(' ', $card['name']);
			list($fname, $lname) = $name;
			list($month, $year) = $expiration;

			Card::create([
				'user_id' => 2,
				'expiration_month'=> $month,
				'expiration_year'=> $year,
				'first_name'=> $fname,
				'last_name'=> $lname,
				'security_code'=> $faker->randomNumber(3),
				'identifier'=> $faker->name(),
				'type' => $card['type'],
				'last_four' => $card['number']
			]);
		}
	}

}