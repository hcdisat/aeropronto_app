<?php


class CityTableSeeder extends Seeder {

	public function run()
	{
		$cities = [
			'Azua',
			'Bahoruco',
			'Barahona',
			'Dajabón',
			'Duarte',
			'Elías Piña',
			'El Seibo',
			'Espaillat',
			'Hato Mayor',
			'Independencia',
			'La Altagracia',
			'La Romana',
			'La Vega',
			'María Trinidad Sánchez',
			'Monseñor Nouel',
			'Montecristi',
			'Monte Plata',
			'Pedernales',
			'Peravia',
			'Puerto Plata',
			'Hermanas Mirabal',
			'Samaná',
			'Sánchez Ramírez',
			'San Cristóbal',
			'San José de Ocoa',
			'San Juan',
			'San Pedro de Macorís',
			'Santiago',
			'Santiago Rodríguez',
			'Santo Domingo',
			'Valverde',
			'Distrito Nacional ',
		];

		foreach($cities as $city)
		{
			City::create([
				'name' => $city
			]);
		}
	}

}