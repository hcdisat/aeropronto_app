<?php


class PhoneTypeTableSeeder extends Seeder {

	public function run()
	{
		$types = [
			'Casa',
			'Trabajo',
			'Movil',
			'Fax',
		];

		foreach($types as $index)
		{
			PhoneType::create([
				'name' => $index
			]);
		}
	}

}