<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TicketsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Ticket::create([
				'package_id' => $index,
				'ticket_status_id' => 1,
				'ticket_priority_id' => $index % 2 == 0 ? 2 :1,
				'user_id' => 4,
				'description' => $faker->text(),
				'subject' => $faker->text(30)
			]);
		}
	}

}