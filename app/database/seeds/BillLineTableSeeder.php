<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BillLineTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			BillLine::create([
				'bill_id' => 1,
				'concept' => $faker->text(),
				'package_id' => $index,
				'total' => $faker->randomFloat(),
				'taxes' => $faker->randomFloat(),
			]);
		}
	}

}