<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BillTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Bill::create([
				'user_id' => 2,
				'deliver_method_id' => 1,
				'total' => $faker->randomFloat(),
				'taxes' => $faker->randomFloat(),
				'code' => $faker->text(),
			]);
		}
	}

}