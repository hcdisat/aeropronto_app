<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ConfigurationTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

        Configuration::create([
            'name' => 'peso',
            'value' => 152,
            'description' => 'Valor de libra predeterminado'
        ]);

        Configuration::create([
            'name' => 'we',
            'value' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus
                        adipiscing, posuere lectus et, fringilla augue.",
            'description' => 'Quienes somos (texto)'
        ]);
        Configuration::create([
            'name' => 'mission',
            'value' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus
                        adipiscing, posuere lectus et, fringilla augue.",
            'description' => 'nuestra mision(texto)'
        ]);
        Configuration::create([
            'name' => 'vision',
            'value' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus
                        adipiscing, posuere lectus et, fringilla augue.",
            'description' => 'nuestra vision(texto)'
        ]);
        Configuration::create([
            'name' => 'values',
            'value' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus
                        adipiscing, posuere lectus et, fringilla augue.",
            'description' => 'nuestros valores(texto)'
        ]);

        Configuration::create([
            'name' => 'mensage_registro',
            'value' => "Gracias por registrarse en Aeropronto!. Verifique su correo para teminar el proceso",
            'description' => 'Mensage de bienvenida luego del registro del usuario'
        ]);

        Configuration::create([
            'name' => 'taxes',
            'value' => "0.18",
            'description' => 'ITEBIS'
        ]);

        Configuration::create([
            'name' => 'uploads_path',
            'value' => "img/slider/",
            'description' => 'Directorio para guardar los archivos al servidor'
        ]);

	}

}