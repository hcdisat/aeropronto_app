<?php

    class SlidersTableSeeder extends Seeder {

	public function run()
	{
		$path = rtrim(app_path(), 'app').'public/img/slider';

		foreach(File::files($path) as $file)
		{
            $img = File::name($file).'.'.File::extension($file);
            Slider::create([
                'image' => $img
            ]);
		}
	}

}