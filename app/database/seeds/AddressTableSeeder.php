<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AddressTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Address::create([
				'user_id' => 2,
				'city_id' => $faker->numberBetween(1, 32),
				'streat' => $faker->streetName,
				'number' => $faker->randomNumber(4)
			]);
		}
	}

}