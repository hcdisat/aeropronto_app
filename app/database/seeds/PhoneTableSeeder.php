<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PhoneTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Phone::create([
				'user_id' => 2,
				'phone_type_id' => $faker->numberBetween(1, 3),
				'number' => $faker->phoneNumber
			]);
		}
	}

}