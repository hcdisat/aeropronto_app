<?php


class TicketPrioritiesTableSeeder extends Seeder {

	public function run()
	{

		$priorities = [
			[
				'name' => 'Baja',
				'description' => 'Prioridad mas baja asignada a un ticket'
			],
			[
				'name' => 'Media',
				'description' => 'Prioridad mas media asignada a un ticket'
			],
			[
				'name' => 'Alta',
				'description' => 'Prioridad mas alta asignada a un ticket'
			]
		];

		foreach($priorities as $priority)
		{
			TicketPriority::create($priority);
		}
	}

}