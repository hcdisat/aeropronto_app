<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

        $user = User::create([
            'fname' => 'Admin',
            'lname' => 'Root',
            'email' => 'admin@aeropronto.info',
            'password' => 'secret',
            'confirmed' => true
        ]);

        $user->roles()->attach(1);
        $user->roles()->attach(2);
        $user->roles()->attach(3);


        foreach (range(1, 20) as $index) {
            $user = User::create([
                'fname' => $faker->firstName(),
                'lname' => $faker->lastName(),
                'email' => $faker->email(),
                'password' => 'secret',
                'confirmed' => true
            ]);

            $user->addRole(3);
        }


        foreach (range(1, 10) as $index) {
            $user = User::create([
                'fname' => $faker->firstName(),
                'lname' => $faker->lastName(),
                'email' => $faker->email(),
                'password' => 'secret',
                'confirmed' => true
            ]);

            $user->addRole(3);
        }

        foreach (range(1, 4) as $index) {
            $user = User::create([
                'fname' => $faker->firstName(),
                'lname' => $faker->lastName(),
                'email' => $faker->email(),
                'password' => 'secret',
                'confirmed' => true
            ]);

            $user->addRole(2);
        }
	}

}