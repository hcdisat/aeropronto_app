<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillLinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bill_lines', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bill_id')->unsigned();
			$table->text('concept');
			$table->integer('package_id')->unsigned();
			$table->decimal('total');
			$table->decimal('weight');
			$table->decimal('taxes');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bill_lines');
	}

}
