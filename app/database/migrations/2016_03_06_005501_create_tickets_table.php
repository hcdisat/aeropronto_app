<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('package_id')->unsigned()->nullable();
			$table->integer('ticket_status_id')->unsigned();
			$table->integer('ticket_priority_id')->unsigned()->nullable();
			$table->integer('user_id')->unsigned();
			$table->boolean('is_asigned')->nullable()->default(false);

			$table->text('description');
			$table->string('subject');

			$table->timestamps();

			$table->foreign('ticket_status_id')	->references('id')->on('ticket_statuses');
			$table->foreign('ticket_priority_id')	->references('id')->on('ticket_priorities');
			$table->foreign('user_id')->references('id')->on('users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
