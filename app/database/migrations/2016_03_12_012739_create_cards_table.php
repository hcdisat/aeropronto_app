<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cards', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('code', 50);
			$table->string('last_four', 50);
			$table->string('expiration_month', 2);
			$table->string('expiration_year', 2);
			$table->string('type', 20);
			$table->string('first_name');
			$table->string('last_name');
			$table->string('security_code', 5);
			$table->string('identifier', 20);

			$table->foreign('user_id')->references('id')->on('users')->OnDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cards');
	}

}
