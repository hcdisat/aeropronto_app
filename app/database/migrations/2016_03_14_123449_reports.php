<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reports extends Migration
{

	protected $path;

	/**
	 * Reports constructor.
	 * @internal param $path
	 */
	public function __construct()
	{
		$this->path = app_path().DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;
	}

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$this->executeStatement($this->path.'add');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$this->executeStatement($this->path.'remove');
	}

	private function executeStatement($dir)
	{
		if (File::isDirectory($dir)) {
			foreach (File::files($dir) as $file) {
				$report = file_get_contents($file);
				DB::statement($report);
			}
		}
	}

}
