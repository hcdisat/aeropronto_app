CREATE OR REPLACE VIEW package_reports AS 
SELECT
  p.id, p.user_id, s.name as estado, p.tracking, p.created_at,
  CONCAT(u.fname, ' ', u.lname) as client
FROM packages p
  INNER JOIN package_status ps ON p.id = ps.package_id
  INNER JOIN statuses s ON ps.status_id = s.id AND ps.status_id = 7
  INNER JOIN users u ON p.user_id = u.id
WHERE p.status_id = 7;

