CREATE OR REPLACE VIEW package_all_reports AS
SELECT
  p.id,   CONCAT(u.fname, ' ', u.lname) as client,
  s.name as estado, p.tracking, p.created_at
FROM packages p
  INNER JOIN statuses s ON p.status_id = s.id
  INNER JOIN users u ON p.user_id = u.id;