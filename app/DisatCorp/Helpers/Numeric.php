<?php

class Numeric
{
    public static function payment($quantity) {

        return $quantity
            ? number_format($quantity, 2, '.', ',')
            : '0.00';
    }

    public static function totalWeight($weight) {

        return $weight
            ? number_format($weight, 2, '.', ',')
            : 'N/A';
    }
}