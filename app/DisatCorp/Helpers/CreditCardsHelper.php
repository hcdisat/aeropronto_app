<?php namespace DisatCorp\Helpers;


use Inacho\CreditCard;

class CreditCardsHelper
{
    protected $card;

    /**
     * CreditCardsHelper constructor.
     * @param $card
     */
    public function __construct()
    {

    }


    /**
     * get credit card type
     * @param $code
     * @return mixed
     */
    public function getCardType($code)
    {
        $result = $this->validate($code);
        return $result->valid
            ? $result->type
            : null;
    }


    public function isValid($code)
    {
        return $this->validate($code)->valid;
    }


    /**
     * @param $code
     * @return object
     */
    public function validate($code)
    {
        return (object) CreditCard::validCreditCard($code);
    }

    /**
     * @param $cva
     * @param $type
     * @return bool
     */
    public function validateCva($cva, $type)
    {
        return CreditCard::validCvc($cva, $type);
    }

    /**
     * @param $year
     * @param $month
     * @return bool
     */
    public function validExpirationDate($year, $month)
    {
        return CreditCard::validDate($year, $month);
    }

}