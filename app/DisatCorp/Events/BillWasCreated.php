<?php namespace DisatCorp\Events;


class BillWasCreated
{
    public $bill;

    /**
     * BillWasCreated constructor.
     * @param $bill
     */
    public function __construct(\Bill $bill)
    {
        $this->bill = $bill;
    }


}