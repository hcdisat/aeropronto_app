<?php namespace DisatCorp\Events;


class CommentWasPosted
{
    public $comment;

    /**
     * CommentWasPosted constructor.
     * @param $comment
     */
    public function __construct(\TicketComment $comment)
    {
        $this->comment = $comment;
    }
}