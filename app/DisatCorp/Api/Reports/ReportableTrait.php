<?php namespace DisatCorp\Api\Reports;

use Configuration;
use DisatCorp\Commands\NewBillCommand;
use Eloquent;
use Illuminate\Support\Facades\View;

trait ReportableTrait
{
    /**
     * @param $package
     * @param $view
     * @return View Object
     */
    public function getBill(Eloquent $package, $view)
    {
        $payment = $package->items->sum('price');
        $weight = $package->items->sum('weight');
        $now = date('D, d/M/Y');

        $this->execute(new NewBillCommand(
            $package->id,
            ($package->items->sum('price') * Configuration::getConfig('taxes')->value),
            $payment,
            $weight,
            false
        ));

        $paymethod = 'Personal';
        $data = compact('package', 'payment', 'weight', 'data', 'now', 'paymethod');
        return \PDF::loadView($view, $data);
    }
}