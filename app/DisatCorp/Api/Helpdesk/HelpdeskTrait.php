<?php namespace DisatCorp\Api\Helpdesk;


trait HelpdeskTrait
{

    public function addTicket($ticcket)
    {
        return $this->asignedTickets()->attach($ticcket);
    }

    public function removeAsignament($ticket)
    {
        return $this->asignedTickets()->detach($ticket);
    }

    public function removeAlAsignaments()
    {
        foreach($this->asignedTickets as $ticket) {
            $this->asignedTickets($ticket);
        }
    }

    public function addTickets(array $tickets)
    {
        foreach($tickets as $ticket) {
            $this->addTicket($ticket);
        }
    }
}