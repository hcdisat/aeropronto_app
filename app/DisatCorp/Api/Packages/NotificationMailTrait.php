<?php namespace DisatCorp\Api\Packages;

use Illuminate\Support\Facades\Mail;

trait NotificationMailTrait {

    /**
     * send an email when package is created
     * @param $package
     */
    public static function onReceivedPackage($package)
    {
        if($package)
        {
            self::mailer($package, 'emails.notifications.received');
        }

    }

    /**
     * sends an email when the package is ready to deliver
     * @param $package
     */
    public static function onReadyStatusPackage($package)
    {
        if($package)
        {
            self::mailer($package, 'emails.notifications.ready');
        }
    }

    /**
     * @param $package
     * @param $view
     */
    private static function mailer($package, $view)
    {
        $data = ['package' => $package];
        Mail::send($view, $data, function ($message) use ($package) {
            $message->to($package->user->email, $package->user->getFullname())
                ->subject('Paquete Disponible (PD)');
        });
    }
}