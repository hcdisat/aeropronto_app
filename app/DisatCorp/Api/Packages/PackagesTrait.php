<?php namespace DisatCorp\Api\Packages;

use Status;

trait PackagesTrait {

    /**
     * Checks if packages has a status
    */
//    public function hasStatus($status)
//    {
//        if($status =  $this->validateStatus($status))
//        {
//            foreach($this->statuses as $sts) {
//                if($sts->id == $status->id) return true;
//            }
//        }
//        return false;
//    }

    public function hasStatus($status)
    {
        return $this->statuses->contains($status);
    }

    public function addStatus($status)
    {
        return $this->statuses()->attach($status);
    }

    public function removeStatus($status)
    {
        return $this->statuses()->detach($status);
    }


    public function updateStatuses($status)
    {
        if($status =  $this->validateStatus($status))
        {
            if($status->id > $this->status_id)
            {
                $statuses = Status::where('order', '<=', $status->order)->get();
                $statuses->each(function($sts){
                    if( !$this->hasStatus($sts)){
                        $this->addStatus($sts);
                    }
                });
            } else {
                $statuses = Status::where('order', '>=', $status->order)->get();
                $statuses->each(function($sts) use ($status) {
                    if($this->hasStatus($sts) and $sts->order != 1) {
                        $this->removeStatus($sts);
                    }
                });
            }

            return true;
        }
        return false;
    }

    /**
     * @param $status
     * @return bool|Status
     */
    private function validateStatus($status)
    {
        $status = $status instanceof Status
            ? $status : Status::find($status);

        if (!$status) return false;
        return $status;
    }
} 