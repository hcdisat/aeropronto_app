<?php namespace DisatCorp\Listeners;


use DisatCorp\Events\BillWasCreated;
use DisatCorp\Events\CommentWasPosted;
use Illuminate\Mail\Mailer;
use Laracasts\Commander\Events\EventListener;
use Monolog\Logger;

class EmailNotifier extends EventListener
{
    protected $mailer;

    /**
     * EmailNotifier constructor.
     * @param $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public function whenCommentWasPosted(CommentWasPosted $event)
    {
        $sendMail = function(\TicketComment $comment, $user)
        {
            $data = ['comment' => $comment];
            $view = \Config::get('helpdesk.email.email_view');
            $this->mailer->send($view, $data, function ($message) use ($comment, $user) {
                $message->to($user->email, $user->getFullname())
                    ->subject('Nuevo mensaje en su ticket #'.$comment->ticket->id);
            });
        };

        $comment = $event->comment;
        if( !$comment->user->isAdmin() )
        {
            foreach ($comment->ticket->users as $user) {
                $sendMail($comment, $user);
            }
            return;
        }

        $sendMail($comment, $comment->ticket->user);
        foreach ($comment->ticket->users as $user)
        {
            if( $comment->user->id == $user->id)
                continue;

            $sendMail($comment, $user);
        }
        return;
        
    }


}