<?php namespace DisatCorp\Commands;


use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;
use TicketComment;

/**
 * Class NewCommentCommandHandler
 * @package DisatCorp\Commands
 */
class NewCommentCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    /**
     * Handle the command
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $data = [
            'comment' => $command->comment,
            'user_id' => $command->user_id,
            'ticket_id' => $command->ticket_id,
        ];

        $comment = TicketComment::post($data);

        $this->dispatchEventsFor($comment);

        return $comment;
    }
}