<?php namespace DisatCorp\Commands;

use Configuration;
use Item;
use Laracasts\Commander\CommandHandler;

class AddItemCommandHandler implements CommandHandler {

    /**
     * Handle the command.
     *
     * @param object $command
     * @return mixed|static
     */
    public function handle($command)
    {
        if( !isset($command->packageId) )
            return null;

        $pound = Configuration::getPricePerPound()->value;

        $price = ($pound * $command->weight);

        $item = Item::create([
            'price' => $price,
            'package_id' => $command->packageId,
            'name' => $command->name,
            'description' => $command->description,
            'weight' => $command->weight

        ]);

        return $item;
    }
}