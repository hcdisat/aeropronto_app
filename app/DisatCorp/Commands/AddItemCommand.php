<?php namespace DisatCorp\Commands;

class AddItemCommand
{

    public $weight;
    public $name;
    public $description;
    public $packageId;

    /**
     * AddItemCommand constructor.
     * @param $weight
     * @param $name
     * @param $description
     * @param $package_id
     * @internal param $packageId
     */
    public function __construct($weight, $name, $description, $package_id)
    {
        $this->weight = $weight;
        $this->name = $name;
        $this->description = $description;
        $this->packageId = $package_id;
    }


}