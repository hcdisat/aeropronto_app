<?php namespace DisatCorp\Commands;


use Bill;
use Configuration;
use Illuminate\Support\Facades\Mail;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;
use Package;

class NewBillCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    /**
     * Handle the command
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $package = Package::findOrFail($command->package_id);

        $billData = [
            'user_id' => $package->user->id,
            'package_id' => $package->id,
            'is_online' => $command->isOnline,
            'total' => $command->total,
            'weight' => $command->weight,
            'taxes' => $command->taxes,
        ];

        if( $package->hasStatus(7) )
        {
            return null;

        }

        $bill = Bill::create($billData);

        foreach ($package->items as $item) {

            $bill->billLines()->save(new \BillLine([
                'concept' => $item->description,
                'package_id' => $command->package_id,
                'total' => $item->price,
                'weight' => $item->weight,
                'taxes' => Configuration::getConfig('taxes')->value * $item->price,
            ]));
        }

        $package->statuses()->attach(7);

        $this->mailer($command->total, $command->weight, $package, $bill);
        return $bill;
    }

    public function mailer($payment, $weight, $package, $bill)
    {
        $now = date('D, d/M/Y');

        $paymethod = 'Personal';
        $pdfData = compact('package', 'payment', 'weight', 'data', 'now', 'paymethod');

        $pdf = \PDF::loadView('components.packages.show', $pdfData);

        $data = ['bill' => $bill];
        Mail::send('emails.bills.bill', $data, function($msessage) use($bill, $pdf){

            $msessage->to($bill->user->email, $bill->user->full_name)
                ->subject('Factura por pago a paquete.');

            $msessage->attachData($pdf->output(), $bill->code.'.pdf');

        });
    }
}