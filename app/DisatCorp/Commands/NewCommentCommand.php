<?php namespace DisatCorp\Commands;


class NewCommentCommand
{

    /**
     * comment's body
     * @var string
     */
    public $comment;

    /**
     * comment's owner
     * @var int
     */
    public $ticket_id;

    /**
     * comment's creater
     * @var int
     */
    public $user_id;

    /**
     * NewCommentCommand constructor.
     * @param string $comment
     * @param int $ticket_id
     * @param int $user_id
     */
    public function __construct($comment, $ticket_id, $user_id)
    {
        $this->comment = $comment;
        $this->ticket_id = $ticket_id;
        $this->user_id = $user_id;
    }

}