<?php namespace DisatCorp\Commands;


class NewBillCommand
{

    /**
     * @var \Package
     */
    public $package_id;

    public $taxes;


    public $total;

    public $weight;

    /**
     * @var bool
     */
    public $isOnline;

    /**
     * NewBillCommand constructor.
     * @param $package_id
     * @param $taxes
     * @param $total
     * @param $weight
     * @param bool $isOnline
     */
    public function __construct($package_id, $taxes, $total, $weight, $isOnline = false)
    {
        $this->package_id = $package_id;
        $this->taxes = $taxes;
        $this->weight = $weight;
        $this->total = $total;
        $this->isOnline = $isOnline;
    }


}