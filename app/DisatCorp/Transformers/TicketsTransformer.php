<?php namespace DisatCorp\Transformers;

use Illuminate\Database\Eloquent\Collection;

class TicketsTransformer
{

    protected $data;
    protected $fields = [
        'id',
        'status',
        'priority',
        'asigned',
        'subject',
        'created_at',
    ];


    public function clientGetTransform(\Ticket $ticket)
    {
        $tracking =  empty($ticket->package->tracking)
            ? 'N/A' : $ticket->package->tracking;

        return [
            $ticket->id,
            $ticket->status->name,
            $ticket->priority->name,
            $ticket->is_asigned ? 'Si' : 'No',
            $ticket->subject,
            $ticket->created_at->format('m/d/Y'),
            $tracking
        ];
    }


    public function clientTransformCollection(Collection $tickets)
    {
        $result = [];

        foreach ($tickets as $ticket) {
            $result[] = $this->clientGetTransform($ticket);
        }

        return array_values($result);
    }

    public function formatUserList(Collection $users)
    {
        $results = [];

        foreach ($users as $user) {
            $results[$user->user_id] = $user->full_name;
        }

        return $results;
    }

}