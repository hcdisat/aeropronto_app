<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class BillLinesForm  extends FormValidator {

    protected $rules = [
        'bill_id' => 'required',
        'concept' => 'required',
        'package_id' => 'required',
        'total' => 'required',
        'taxes' => 'required',
    ];
} 