<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class BillForm  extends FormValidator {

    protected $rules = [
        'user_id' => 'required',
        'taxes' => 'required',
        'total' => 'required',
        'code' => 'required',
    ];
} 