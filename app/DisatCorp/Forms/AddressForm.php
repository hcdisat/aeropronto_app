<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class AddressForm extends FormValidator {

    protected $rules = [
        'user_id' => 'required',
        'city_id' => 'required',
        'streat' => 'required',
        'number' => 'required',
    ];
} 