<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class LoginForm  extends FormValidator {

    protected $rules = [
        'email' => 'required|email|max:200',
        'password' => 'required|min:4|max:200'
    ];
} 