<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class CardForm extends FormValidator {

    protected $rules = [
        'user_id' => 'required',
        'last_four'=> 'required',
        'expiration_month'=> 'required',
        'expiration_year'=> 'required',
        'first_name'=> 'required',
        'last_name' => 'required',
        'security_code'=> 'required|min:3|max:3',
        'identifier'=> 'required|max:20',
    ];

} 