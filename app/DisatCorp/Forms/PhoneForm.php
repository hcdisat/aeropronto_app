<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class PhoneForm extends FormValidator {

    protected $rules = [
        'user_id' => 'required',
        'phone_type_id' => 'required',
        'number' => 'required',
    ];
} 