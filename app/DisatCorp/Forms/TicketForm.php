<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class TicketForm extends FormValidator {

    protected $rules = [
        'subject' => 'required',
        'description' => 'required',
        'ticket_status_id' => 'required',
        'user_id' => 'required',
        'ticket_priority_id' => 'required',
    ];

    protected $rules_edit = [
        'ticket_status_id' => 'required',
        'ticket_priority_id' => 'required',
    ];

    public function setEditRules()
    {
        $this->rules = $this->rules_edit;
    }

} 