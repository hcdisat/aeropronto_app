<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class ProfileForm  extends FormValidator {

    /**
     * TODO: Implement this
    **/
    protected $rules = [
        'address' => 'Some Street #66, Sector, City, Country',
        'user_id' => 1,
        'email' => 'hcdisat@gmail.com',
        'phone_one' => '1000-000-0000',
        'phone_two' => '1000-000-0000',
        'city' =>  'Santo Domingo DN'
    ];
} 