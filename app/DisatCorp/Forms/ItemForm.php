<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class ItemForm extends FormValidator {

    protected $rules = [
        'name' => 'required|max:100',
        'weight' => 'required|numeric',
        'description' => 'required',
    ];
} 