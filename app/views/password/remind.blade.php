@extends('layouts.master')
@section('title')
@parent    
@stop
@section('body')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recupere su contraseña</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['role' => 'form', 'method' => 'post']) }}                        
                            <fieldset>
                                <div class="form-group">
                                    {{ Form::email('email', null, 
                                        [
                                            'class' => 'form-control', 
                                            'placeholder' => 'E-mail', 
                                            'required' => 'required',
                                            'autofocus'
                                        ]) 
                                    }}
                                    {{ $errors->first('email', '<span class="text-danger">:message</span>') }}                                             
                                </div>                                                                
                                <button type="submit" class="btn btn-lg btn-success btn-block">Reset</button>
                            </fieldset>
                        @if(Session::has('flash_message'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('flash_message') }}</p>    
                            </div>                            
                        @endif     
                        @if(Session::has('error'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('error') }}</p>    
                            </div>                            
                        @endif     
                        @if(Session::has('status'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('status') }}</p>    
                            </div>                            
                        @endif                        
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop