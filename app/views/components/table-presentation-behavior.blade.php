{{ HTML::script('bower_components/bootstrap-select/js/bootstrap-select.js') }}
{{ HTML::script('bower_components/datatables/media/js/jquery.dataTables.js') }}
{{ HTML::script('bower_components/bootstrap-modal/js/bootstrap-modalmanager.js') }}
{{ HTML::script('bower_components/bootstrap-modal/js/bootstrap-modal.js') }}
{{ HTML::script('bower_components/datatables-responsive/js/dataTables.responsive.js') }}
{{ HTML::script('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}
<script>
    $('#packageTable').DataTable({
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });

    $('#itemsTable_filter input').addClass('form-control input-sm');
</script>