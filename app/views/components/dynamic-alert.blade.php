<br>
<div class="alert alert-{{$level}}">
	<button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
	<h4>{{$message}}</h4>
</div>	