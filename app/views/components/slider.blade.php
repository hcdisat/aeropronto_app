<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading">
			Ofertas
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">								 
				<!-- Carousel items -->
				<div class="carousel-inner">
				   {{HTML::slider()}}			
				</div>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#myCarousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="carousel-control right" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
			<!-- /.panel-body -->
		</div>
	</div>	
</div>