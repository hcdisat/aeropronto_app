{{ HTML::style('bower_components/bootstrap-select/dist/css/bootstrap-select.css') }}
{{ HTML::style('bower_components/datatables/media/css/jquery.dataTables.css') }}
{{ HTML::style('bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}
{{ HTML::style('bower_components/bootstrap-modal/css/bootstrap-modal.css') }}
{{ HTML::style('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}
{{ HTML::style('bower_components/datatables-responsive/css/dataTables.responsive.css') }}
<style>
    body { font-size: 140% }

    table.dataTable th,
    table.dataTable td {
        white-space: nowrap;
    }
</style>