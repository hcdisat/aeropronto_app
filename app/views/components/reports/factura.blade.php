@extends('components.reports.main')
@section('report')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-sm-6">
						<p>Factura: F-PSDQ-69854</p>
						<p>Fecha: 27/58/74</p>
					</div>
					<div class="col-sm-6">
						<p>Nombre: Nombre Completo</p>
						<p>Cuenta: CLI-SDQ-44171</p>
					</div>
				</div>            
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<h3>Estado del Paquete: Estado</h3>    
				<table class="table table-striped table-bordered table-hover items" id="itemsTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Tracking no.</th>
							<th>Nombre</th>
							<th>Peso</th>
							<th>Cobro</th>							
						</tr>
					</thead>
					<tbody>    						
						<tr id="test">  
							<td>p-sdq-bar</td>
							<td>Foo</td>
							<td role="weight">5 LBS</td>
							<td role="price">$RD 45895.58</td>							
						</tr>						
					</tbody>
				</table>      				
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
			<div class="panel-footer">
            <div class="row">                         
              <div class="col-md-4 pull-right text-right">
                <span>Cobro total</span>
                <h4 id="total-price">$RD5,000.00</h4>
              </div>
               <div class="col-md-4 pull-right text-right">
                <span>Peso total</span>
                <h4 id="total-weight">25 LBS</h4>
              </div>
            </div>
          </div>
      </div>
      <!-- /.panel -->      
  </div>
@stop