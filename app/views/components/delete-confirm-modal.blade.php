<script>

    $(document).ready(function(){

        var confirm = new Modal('confirm');
        confirm.setTitle('Confirmar');
        confirm.setBody($('<p>').html('Seguro desea eliminar el item?'));
        confirm.setActionButton('btn-danger', {
            'type': 'submit'
        }, 'Borrar');

        var modal = confirm.renderModal();

        var form = $('<form>');

        var token =
                $('<input>', {
                    'type': 'hidden',
                    'name': 'csrf_token',
                    'value': '{{ csrf_token() }}}'
                });

        var hiddenInput =
                $('<input>', {
                    'name': '_method',
                    'type': 'hidden',
                    'value': 'delete'
                });

        form.append(token, hiddenInput).appendTo('body');
        modal.appendTo(form);

        $('.fa.fa-trash.fa-2x').click(function(){
            form.attr('action', $(this).data('uri'));
            form.attr('method', $(this).data('method'));
            console.log($(this));
        });
    });

</script>