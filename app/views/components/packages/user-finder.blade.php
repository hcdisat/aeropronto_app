{{ Form::text('find-user', null, 
  [
    'class' => 'form-control', 'id' => 'find-user',
    'autocomplete' => 'off',
    'required' => 'required',
    'data-validation' => 'required'
  ]) 
}}              
{{ Form::hidden('user_id', null,['id' => 'user_id']) }}