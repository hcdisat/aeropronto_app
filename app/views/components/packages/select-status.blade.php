{{ Form::open(['route' => 'package.update', 'id' => 'changeStatus', 'method' => 'PUT']) }}     
<div class="col-md-6">         
  <span>Estado</span>
  <br/>                 
  <select class="selectpicker" name="status"
  data-package="{{$package->id}}" data-url="/package/" data-method="PUT">
  @foreach($statuses as $key => $value)              
  <option value="{{$key}}" {{$key == $package->status_id ? 'selected="selected"': ''}}>
    {{$value}}
  </option>
  @endforeach                            
</select>
</div>
{{ Form::close() }}