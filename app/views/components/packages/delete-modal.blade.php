<!-- Modal -->
<div class="modal fade" id="delete-package" tabindex="-1" role="dialog" aria-labelledby="delete-dialoglLabel" aria-hidden="true">
    <div class="">
        {{ Form::open(['url' => url('/auth/validate'), 'method' => 'post']) }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Super Administrador</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Correo de administrador</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <input type="hidden" name="package_id" id="edit-package">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Entrar</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
