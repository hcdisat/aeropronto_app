<tr id="row-{{ $package->id}}">
    <td><i class="pointer fa fa-plus-square-o"> <a href="{{route('package.show', $package->id)}}">P-SDQ-0{{ $package->id}}</a></td>
    <td>{{ $package->user->getFullName()}}</td>
    <td>{{ $package->status->name or 'N/A'}}</td>
    <td role="weight">
        {{ HTML::totalWeight($package->items->sum('weight')) }} LBS
    </td>
    <td>{{ $package->items->count()}}
        {{ $package->items->count() == 1
            ? 'Item'
            : 'Items'
        }}
    </td>
    <td >$RD {{ HTML::payment($package->items->sum('price'))}}
    </td>
    <td>{{ $package->created_at->format('d/m/Y') }}</td>
    <td>
        <div class="row">
            <div class="col-md-12 pull-right">
                {{--<button type="button" data-toggle="modal" data-target=".bs-edit-package"--}}
                        {{--class="btn btn-info btn-circle" data-package="{{$package->id}}" >--}}
                    {{--<i class="fa fa-th-list"></i>--}}
                {{--</button>--}}

                {{--<button  data-toggle="modal" data-target="#delete-package"--}}
                         {{--type="button" data-package-id="{{$package->id}}"--}}
                         {{--class="btn btn-danger btn-circle"><i class="fa fa-minus"></i>--}}
                {{--</button>--}}
                <button data-toggle="modal" data-target="#myModal"
                        type="button" data-package="{{ $package->id }}"
                        class="btn btn-success btn-circle">
                    <i class="fa fa-plus"></i>
                </button>
                <button  data-toggle="modal" data-target="#delete-package"
                         type="button" data-package-id="{{$package->id}}"
                         class="btn btn-default btn-circle"><i class="fa fa-pencil"></i>
                </button>
            </div>
        </div>
    </td>
</tr>