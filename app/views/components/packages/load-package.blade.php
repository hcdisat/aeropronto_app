<div class="modal fade bs-edit-package" data-width="960" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

  <div class="">
    <div class="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
        </button>        
        <h4 class="modal-title" id="myModalLabel">Editar Paquete PSDQ-{{$package->id}}</h4>
      </div>
      <div class="modal-body">
        <div class="row">        
          {{ Form::open(['route' => 'package.update', 'id' => 'changeStatus', 'method' => 'PUT']) }}     
          <div class="col-md-6">         
            <span>Estado</span>
            <br/>                 
            <select class="selectpicker" name="status" id="packageStatus"
            data-url="/package/" data-method="PUT" data-package="{{$package->id}}">
            @foreach($statuses as $key => $value)              
            <option value="{{$key}}" {{$key == $package->status_id ? 'selected="selected"': ''}}>
              {{$value}}
            </option>
            @endforeach                            
          </select>
        </div>
        {{ Form::close() }}
        <div class="col-md-6 pull-right">
          <div id="panel-notify">            
          </div>
        </div>
      </div>
      <br>
      <div class="panel panel-default">
        <div class="panel-heading">
          Items
        </div>        
        <!-- /.panel-heading -->
        <div class="panel-body">
          @if($package->items->count() > 0)
          <table class="table table-striped table-bordered table-hover items" id="itemsTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Peso</th>
                <th>Precio</th>
                <th class="text-right">Acciones</th>
              </tr>
            </thead>
            <tbody>    
              @foreach($package->items as $item)
                @include('components.items.items-rows', $item)      
              @endforeach
            </tbody>
          </table>
          @else
          <p class="text-info">No Existen items!</p>
          @endif          
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <!--div class="row">            
              <div class="col-md-4">
                <span>Peso total</span>
                <h4 id="total-weight">876</h4>
              </div>
              <div class="col-md-4">
                <span>Precio total</span>
                <h4 id="total-price">$RD876.99</h4>
              </div>
            </div>
          </div>
        </div-->
        <!-- /.panel -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>
      </div>
    </div>
  </div>
</div>