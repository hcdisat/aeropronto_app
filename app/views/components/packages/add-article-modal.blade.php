<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Items</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['role' => 'addItem', 'method' => 'post']) }}
                    @include('items.add')
                {{ Form::close() }}
            </div>
            <span class="alert-danger hide">Este campo es requerido</span>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnAddItem" data-url="items">Guardar</button>
            </div>
        </div>
    </div>
</div>