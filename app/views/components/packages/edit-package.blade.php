@section('css') {{HTML::style('bower_components/bootstrap-select/dist/css/bootstrap-select.css')}} @stop
<div class="modal fade bs-edit-package" data-width="960" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

  <div class="">
    <div class="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Editar Paquete {{$package->tracking}}</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="status-holder">

          </div>
          <div class="col-md-6 pull-right">
            <div id="panel-notify">
            </div>
          </div>
        </div>
        <br>
        <div class="panel panel-default">
          <div class="panel-heading">
            Items
          </div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div id="table-container" role="items-table">
              <!--Table here-->
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.panel-body -->
          <div class="panel-footer">
            <!--div class="row">            
              <div class="col-md-4">
                <span>Peso total</span>
                <h4 id="total-weight">876</h4>
              </div>
              <div class="col-md-4">
                <span>Precio total</span>
                <h4 id="total-price">$RD876.99</h4>
              </div>
            </div>
          </div>
        </div-->
            <!-- /.panel -->
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div></div>