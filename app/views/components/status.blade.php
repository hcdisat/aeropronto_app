 <div class="col-lg-12 col-md-6">
<div class="panel panel-{{$status->css_class}}">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-3">
				<i class="fa fa-check fa-5x"></i>
			</div>
			<div class="col-xs-9 text-right">
				<div class="huge">{{$status->abbr}}</div>
				<div>{{$status->name}}</div>
			</div>
		</div>
	</div>
	<a href="#">
		<div class="panel-footer">			
			<div class="panel panel-{{$status->css_class}}">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#{{'collapse' . $status->order}}">
							<span class="pull-left">+Info</span>
			<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
			<div class="clearfix"></div>
						</a>
					</h4>
				</div>	
				<div id="{{'collapse' . $status->order}}" class="panel-collapse collapse in">	
					<div class="panel-body">
						{{$status->description}}
					</div>
				</div>
			</div>
		</div>
	</a>
</div>
</div>