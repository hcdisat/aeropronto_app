<!-- Modal -->
<div class="modal fade" id="delete-user"
    tabindex="-1" role="dialog" aria-labelledby="delete-dialoglLabel" aria-hidden="true">
    <div class="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmar borrado de usuario</h4>
            </div>
            <div class="modal-body">
                <p>¿Está seguro que desea borrar el usuario?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button data-dst-url='/user/' id="del-user"
                        type="button" class="btn btn-danger">Borrar</button>
            </div>
        </div>
    </div>
</div>