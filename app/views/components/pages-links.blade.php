<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">            
            <li>
                <a href="{{route('pages.contact')}}"><i class="fa fa-circle-o fa-fw"></i> Contacto</a>
            </li>
            <li>
                <a href="{{route('pages.about')}}"><i class="fa fa-circle-o fa-fw"></i> Sobre Nosotros</a>
            </li>      
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->