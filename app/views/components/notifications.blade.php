<div id="notify">
	@if(Session::has('notifier.message'))
	<br>
		<div class="alert alert-{{Session::get('notifier.level')}}">
			<button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
			<h4>{{Session::get('notifier.message')}}</h4>
		</div>	
	@endif
</div>