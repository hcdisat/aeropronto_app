<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            @if( Auth::user()->hasRole('service') )
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-user"></i> Servicios
                                </button>
                            </span>

                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="{{route('tickets.index')}}"><i class="fa fa-info fa-fw"></i> Reclamaciones</a>
                </li>
                @yield('sub-menu')
            @else
                <li>
                    <a href="{{route('pages.contact')}}"><i class="fa fa-circle-o fa-fw"></i> Contacto</a>
                </li>
                <li>
                    <a href="{{route('pages.about')}}"><i class="fa fa-circle-o fa-fw"></i> Sobre Nosotros</a>
                </li>
                @if(Auth::user()->hasRole('user'))
                    <li>
                        <a href="{{route('pages.packages')}}"><i class="fa fa-gift fa-fw"></i> Mis Paquetes</a>
                    </li>
                    <li>
                        <a href="{{route('tickets.index')}}"><i class="fa fa-info fa-fw"></i> Reclamaciones</a>
                    </li>
                    @yield('sub-menu')
                @endif
            @endif
        </ul>

    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->