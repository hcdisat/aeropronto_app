<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{route('pages.contact')}}"><i class="fa fa-circle-o fa-fw"></i> Contacto</a>
            </li>
            <li>
                <a href="{{route('pages.about')}}"><i class="fa fa-circle-o fa-fw"></i> Sobre Nosotros</a>
            </li>
            @if(Auth::user()->hasRole('user'))
                <li>
                    <a href="{{route('pages.packages')}}"><i class="fa fa-circle-o fa-fw"></i> Mis Paquetes</a>
                    <a href="{{route('tickets.index')}}"><i class="fa fa-circle-o fa-fw"></i> Reclamación</a>
                </li>
            @endif
            @if(Auth::user()->hasRole('admin'))
                <li>
                    <a href="{{ url('/package') }}"><i class="fa fa-circle-o fa-fw"></i> Paquetes</a>
                </li>
                @if(Auth::user()->hasRole('owner'))
                    <li>
                        <a href="{{ route('user.index') }}"><i class="fa fa-circle-o fa-fw"></i> Ususarios</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o fa-fw"></i> Slider<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                {{link_to_route('settings.slider.index', 'Listado')}}
                            </li>
                            <li>
                                {{link_to_route('settings.slider.create', 'Crear')}}
                            </li>
                            <li>
                                {{link_to_route('settings.slider.reactivate', 'Reactivar')}}
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                @endif
                @yield('links')
                <li><hr/></li>
                <li>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Precio/Libra
                        </div>
                        <div class="panel-body">
                            <h1 class="text-muted">$RD
                                {{HTML::payment(Configuration::getPricePerPound()->value)}}
                            </h1>
                        </div>
                        <div class="panel-footer">
                            Precio/Libra
                        </div>
                    </div>
                </li>
            @endif
                @yield('sub-menu')
        </ul>

    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->