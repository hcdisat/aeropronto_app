<!-- Modal -->
<div class="modal fade" id="add-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Item</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'items.update', 'role' => 'editItem', 'method' => 'PUT']) }}
                {{Form::hidden('package_id', null, ['id' => 'package_id'])}}
                <div class="form-group">
                    <label>Nombre</label>                        
                    {{ Form::text('name', null, ['class' => 'form-control'])}}
                    <p class="help-block">Ejemplo "Telefono Celular".</p>
                </div>
                <div class="form-group">
                    <label>Peso</label>
                    {{ Form::text('weight', null, ['class' => 'form-control'])}}
                    <p class="help-block">Ejemplo: en libras "5lb".</p>
                </div>
                <div class="form-group">
                    {{ Form::hidden('price', null, ['id' => 'price'])}}                    
                </div>
                <div class="form-group">
                    {{ Form::label('description', 'Decripción') }}                        
                    {{ Form::textarea('description', null, ['class' => 'form-control'])}}                        
                </div>
                    <div class="alert-danger hide">
                    <ul id="item-error-display">
                        
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                        
                    <button type="button" class="btn btn-primary" 
                            data-url="/items/" data-type="PUT"
                            id="btnEditItem" data-url="items">Guardar Cambios</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

