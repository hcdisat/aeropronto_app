<tr id="{{'item-row-'.$item->id}}">  
  <td>{{ $item->name}}</td>
  <td role="weight">{{ HTML::totalWeight($item->weight) }} LBS</td>
  <td role="price">$RD {{ HTML::payment($item->price) }}</td>
  <td>
  <div class="pull-right">      
    <button data-id="{{ $item->id }}" type="button" data-url="/items/{id}/edit"
            data-type="GET" data-toggle="modal" data-target="#add-item"
            class="btn btn-info btn-circle">
            <i class="fa fa-pencil"></i>
    </button>    
    <button data-id="{{ $item->id }}" type="button"
            data-toggle="modal" data-target="#delete-item"
            class="btn btn-danger btn-circle">
            <i class="fa fa-minus"></i>
    </button>    
  </div>    
  </td>
</tr>