@if($items->count() > 0)
  <table class="table table-striped table-bordered table-hover items" id="itemsTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Peso</th>
      <th>Precio</th>
      <th class="text-right">Acciones</th>
    </tr>
  </thead>
  <tbody>    
    @foreach($items as $item)
      @include('components.items.items-rows', $item)      
    @endforeach
  </tbody>
</table>
@else
  <p class="text-info">No Existen items!</p>
@endif
