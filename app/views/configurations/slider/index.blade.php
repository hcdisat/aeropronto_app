@extends('layouts.main')
@section('content')
<div class="container-fluid">
	<div class="row">					
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Slider
				</div>
				<div class="panel-body">
					<div class="container-fluid">
						<div class="row">	
							@forelse($sliders as $slider)							
							<div class="col-sm-6 col-md-4">
								<div class="thumbnail">
									{{HTML::image('img/slider/'.$slider->image, $slider->image,
											['class' => 'img-responsive'])}}									
									<div class="caption">									
										<div class="row">
											<div class="col-sm-6 col-md-4">
												<p>{{{$slider->caption}}}</p>
											</div>
										</div>

										<p>
										@if( !$slider->deleted_at)
										<a href="{{route('settings.slider.edit', $slider->id)}}" class="btn btn-primary" role="button">
											<i class="fa fa-pencil"></i>
										</a> 
										<a href="{{route('settings.slider.delete', $slider->id)}}" class="btn btn-danger" role="button">
											<i class="fa fa-trash"></i>
										</a>
										@else
											<a href="{{route('settings.slider.undelete', $slider->id)}}" class="btn btn-info" role="button">
											<i class="fa fa-save"></i> Restaurar
										</a> 
										@endif
										</p>
									</div>
								</div>
							</div>																
							@empty
							<h3>No existen imagenes para el Slide Show</h3>						
							@endforelse  
						</div>	
						<hr/>                      
					</div>
					<div class="panel-footer">
						<div class="row">
							<div class="col-xs-6 col-xs-offset-8">
								{{$sliders->links()}}
							</div>
						</div>
					</div>
				</div>
			</div>						
		</div>
	</div>
</div>
@stop