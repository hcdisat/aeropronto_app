@extends('layouts.main')
@section('css')
{{ HTML::style('../bower_components/bootstrap-fileinput/css/fileinput.min.css') }}
@stop
@section('content')
<div class="container-fluid">
	{{Form::open(['action' => ['SliderController@store'], 'method' => 'post', 'files' => true])}}
	<div class="row">
		<div class="col-lg-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-image fa-fw"></i> Elegir Imagen
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="form-group">
						<label class="label label-info">Seleccione una imagen</label>
						{{Form::file('image', ['class' => 'file', 'data-show-upload' => 'false'])}}
						@if(Session::has('errors'))
						<div class="form-group">
							<p class="text-danger text-center">{{ Session::get('errors')->first() }}</p>
						</div>
						@endif
					</div>
				</div>
				<!-- /.panel-body -->
			</div>
		</div>

		<div class="col-lg-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-pencil fa-fw"></i> Agregar Texto
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-12">
							{{Form::textarea('caption', null, ['class' => 'form-control'])}}
						</div>
					</div>
					<div class="col-md-12">
						&nbsp;
					</div>
					{{Form::submit('Guardar', ['class' => 'btn btn-info btn-block'])}}
				</div>
				<!-- /.panel-body -->
			</div>
		</div>
	</div>
	{{Form::close()}}
</div>
</div>
@stop
@section('js')
{{ HTML::script('../bower_components/bootstrap-fileinput/js/fileinput.min.js') }}
@stop