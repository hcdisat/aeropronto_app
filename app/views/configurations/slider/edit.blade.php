@extends('layouts.main')
@section('css')
{{ HTML::style('../bower_components/bootstrap-fileinput/css/fileinput.min.css') }}
@stop
@section('content')
<div class="container-fluid">
    {{Form::open(['action' => ['SliderController@update', $slider->id], 'method' => 'put', 'files' => true])}}
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-pencil fa-fw"></i> Editar / Slider
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="form-group">
                        <label class="label label-info">Seleccione una imagen</label>
                        {{Form::file('image', ['class' => 'file', 'data-show-upload' => 'false'])}}
                        {{Form::hidden('image_old', $slider->image)}}
                         @if(Session::has('errors'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('errors')->first() }}</p>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-pencil fa-fw"></i> Editar / Texto
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="form-group">
                            {{Form::textarea('caption', $slider->caption, ['class' => 'form-group', 'cols' => 25, 'rows' => 5])}}
                        </div>
                        <div class="thumbnail">
                            {{HTML::image('img/slider/'.$slider->image, $slider->image,
                            ['class' => 'img-responsive'])}}
                            <div class="caption">
                                <p>Imagen Actual</p>
                            </div>
                        </div>
                        {{Form::submit('Guardar', ['class' => 'btn btn-info btn-block'])}}
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}    
</div>
@stop
@section('js')
  {{ HTML::script('../bower_components/bootstrap-fileinput/js/fileinput.min.js') }}
@stop