@extends('layouts.main')
@section('title')
	Configuración | @parent
@stop

@section('content')
	<div class="container-fluid">

		@section('page-title')

			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Configuración</h1>
				</div>
			</div>
		@stop

		<div class="row">
			<div class="col-lg-4">
				{{Form::open(['route' => 'settings.update', 'method' => 'POST'])}}
				<div class="panel panel-default">
					<div class="panel-heading">
						Configuración de Libra
					</div>
					<div class="panel-body">
						<label class="control-label" for="inputSuccess">Costo de 1 libra</label>
						<div class="form-group input-group">
							<span class="input-group-addon">$</span>
							{{Form::text('value', HTML::payment($weight), ['class' => 'form-control'])}}
							{{Form::hidden('id', $id)}}
							<span class="input-group-addon">.00</span>
						</div>
					</div>
					<div class="panel-footer">
						{{Form::submit('Guardar', ['class' => 'btn btn-default'])}}
					</div>
				</div>
				{{Form::close()}}

			</div>
		</div>

		<div class="row">
			{{Form::open(['route' => 'settings.we'])}}
			<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Quienes Somos
					</div>
					<div class="panel-body">
						<div class="form-group">
							{{Form::textarea('value', $we->value, ['class' => 'form-control'])}}
						</div>
					</div>
					<div class="panel-footer">
						{{Form::submit('Guardar', ['class' => 'btn btn-default'])}}
					</div>
				</div>
			</div>
			{{Form::close()}}
			{{Form::open(['route' => 'settings.mission'])}}
			<div class="col-lg-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Misión
					</div>
					<div class="panel-body">
						<div class="form-group">
							{{Form::textarea('value', $mission->value, ['class' => 'form-control'])}}
						</div>
					</div>
					<div class="panel-footer">
						{{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
					</div>
				</div>
			</div>
			{{Form::close()}}
		</div>
		<div class="row">
			{{Form::open(['route' => 'settings.vision'])}}
			<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Visión
					</div>
					<div class="panel-body">
						<div class="form-group">
							{{Form::textarea('value', $vision->value, ['class' => 'form-control'])}}
						</div>
					</div>
					<div class="panel-footer">
						{{Form::submit('Guardar', ['class' => 'btn btn-default'])}}
					</div>
				</div>
			</div>
			{{Form::close()}}
			{{Form::open(['route' => 'settings.values'])}}
			<div class="col-lg-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Valores
					</div>
					<div class="panel-body">
						<div class="form-group">
							{{Form::textarea('value', $values->value, ['class' => 'form-control'])}}
						</div>
					</div>
					<div class="panel-footer">
						{{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
					</div>
				</div>
			</div>
			{{Form::close()}}
		</div>
		{{Form::close()}}
		<div class="row">
			{{Form::open(['route' => 'settings.contact'])}}
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Contacto
					</div>
					<div class="panel-body">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-4">
									{{Form::hidden('id', $contact->id)}}
									<div class="form-group">
										<label class="form-label">Dirección</label>
										{{Form::text('address', $contact->address,['class' => 'form-control'])}}
									</div>
									<div class="form-group">
										<label class="form-label">Correo</label>
										{{Form::text('email', $contact->email,['class' => 'form-control'])}}
									</div>
									<div class="form-group">
										<label class="form-label">Ciudad</label>
										{{Form::text('city', $contact->city,['class' => 'form-control'])}}
									</div>
								</div>
								<div class="col-md-3 col-md-offset-3">
									<div class="form-group">
										<label class="form-label">Teléfono Uno</label>
										{{Form::text('phone_one', $contact->phone_one,['class' => 'form-control'])}}
									</div>
									<div class="form-group">
										<label class="form-label">Teléfono Dos</label>
										{{Form::text('phone_two', $contact->phone_two,['class' => 'form-control'])}}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						{{Form::submit('Guardar', ['class' => 'btn btn-default'])}}
					</div>
				</div>
			</div>
			{{Form::close()}}
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Asuntos Predefinidos para Reclamaciones
						<div class="btn-group pull-right">
							<a href="{{ route('settings.subjects-add') }}" class="btn btn-default btn-xs" >
								<i class="fa fa-plus"></i> Agregar Asunto
							</a>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-hover" id="phone">
								<thead>
								<tr>
									<th>#</th>
									<th>Asunto</th>
									<th>Creando en</th>
									<th>Acciones</th>
								</tr>
								</thead>
								<tbody>
								@foreach($subjects as $subject)
									<tr>
										<td>{{ $subject->id }}</td>
										<td>{{ $subject->subject }}</td>
										<td>{{ $subject->created_at->format('d/m/Y') }}</td>
										<td>
											<div>
												<a href="#" data-toggle="modal" data-target="#confirm">
													<i data-method="post" data-uri="{{ route('settings.subjects-delete', $subject->id) }}" class="fa fa-trash fa-2x"></i>
												</a>
												<a href="{{ route('settings.subjects-edit', $subject->id) }}">
													<i class="fa fa-pencil fa-2x"></i>
												</a>
											</div>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('js')
	@include('components.delete-confirm-modal')
@stop