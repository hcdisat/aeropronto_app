@extends('layouts.main')
@section('css')
{{ HTML::style('bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}
{{ HTML::style('bower_components/bootstrap-modal/css/bootstrap-modal.css') }}
{{ HTML::style('bower_components/datatables/media/css/jquery.dataTables.css') }}
{{ HTML::style('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}
{{ HTML::style('bower_components/datatables-responsive/css/dataTables.responsive.css') }}
@stop
@section('content')
<div class="container-fluid">
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default background">
			<div class="panel-heading">
				Configuración | Usuarios
			</div>
			<div class="panel-body">					
					<table id="userTable" class="table table-striped table-hover dt-responsive" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Email</th>
								<th>Miembro Desde</th>
								<th>Administrador?</th>
								<th class="text-right">Acciones</th>
							</tr>
						</thead>
						<tbody>    
							@foreach($users as $user)
							<tr>
								<td>{{$user->getFullName()}}</td>
								<td>{{$user->email}}</td>
								<td>{{$user->created_at}}</td>
								<td>{{$user->hasRole('admin') ? 'Si' : 'No'}}</td>
								<td>
									<div class="pull-right">      
										<a  href="{{route('user.edit', $user->id)}}" 
											class="btn btn-info btn-circle">
											<i class="fa fa-pencil"></i>
										</a>    
										<button data-dst-target="{{$user->id}}"
												class="btn btn-danger btn-circle"  data-toggle="modal" 
												data-target="#delete-user"> 
											<i class="fa fa-minus"></i>
										</button>    
									</div>    
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<div class="row">

					</div>					
			</div>
		</div>
	</div>
</div>
</div>	
@include('components.delete-user')
@stop

@section('js')
{{ HTML::script('bower_components/datatables/media/js/jquery.dataTables.js') }}
{{ HTML::script('bower_components/datatables-responsive/js/dataTables.responsive.js') }}
{{ HTML::script('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}
{{ HTML::script('bower_components/bootstrap-modal/js/bootstrap-modalmanager.js') }}
<script>
	$('#userTable').DataTable({
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });
</script>
@stop