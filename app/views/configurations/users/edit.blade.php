@extends('layouts.main')
@section('css') {{HTML::style('bower_components/bootstrap-select/dist/css/bootstrap-select.css')}} @stop
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<p>Configuración | Usuarios</p>
		<p>Email: {{$user->email}}</p>
		ID: {{$user->id}}
	</div>
	
	{{ Form::open(['route' => 'user.update', 'method' => 'PUT']) }}
	<div class="row">	
		<div class="col col-md-9">
			<div class="panel-body">

				<div class="form-group">
					<label>Nombre</label>                        
					{{ Form::text('fname', $user->fname, ['class' => 'form-control'])}}                    
				</div>
				<div class="form-group">
					<label>Apellido</label>                        
					{{ Form::text('lname', $user->lname, ['class' => 'form-control'])}}                    
				</div>
				{{Form::hidden('user_id', $user->id)}}
				<div class="panel panel-default">
					<div class="panel-heading">
						Roles
					</div>
					<!-- .panel-heading -->
					<div class="panel-body">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Roles Activos</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in">
									<div class="panel-body">
										<ul>
											@forelse($user->roles as $role)
											<li>{{$role->name}}</li>
											@empty
											<li>No tiene ningun role asignado.</li>
											@endforelse
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Editar Role</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse">
									<div class="panel-body">
										<div class="form-group">		
											<label>Roles</label>
											{{ Form::select('add_roles[]', $roles, $user->roles()->lists('ROLE_ID'), 
												['class' => 'selectpicker', 'multiple'])}}			
										</div> 
									</div>
								</div>
							</div>							
						</div>
					</div>
					<!-- .panel-body -->
				</div>				              
				<div class="panel-footer">
					{{Form::submit('Guardar', ['class' => 'btn btn-default'])}}
				</div>	
			</div>
		</div>
	</div>
	{{Form::close()}}
	@stop
	@section('js')
	{{ HTML::script('bower_components/bootstrap-select/js/bootstrap-select.js') }}
	@stop
