@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8">
                @if(isset($subject))

                    {{ Form::model($subject, [
                        'route' => ['settings.subjects-update', $subject->id],
                        'role' => 'form',
                        'method' => 'put'
                        ]) }}

                    @include('configurations.subjects.fields')
                    {{ Form::close() }}

                @else

                    {{ Form::open(['route' => 'settings.subjects-store', 'role' => 'form']) }}
                    @include('configurations.subjects.fields')
                    {{ Form::close() }}

                @endif
            </div>
        </div>
    </div>
@stop