<div class="panel panel-primary">
    <div class="panel-heading">
        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs">
            <i class="fa fa-arrow-left"></i> Atras
        </a>
        <span class="pull-right">{{ !isset($subject) ? 'Agregar' : 'Editar'}} Asunto</span>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label>Asunto</label>
            {{ Form::textarea('subject', null, ['class' => 'form-control', 'required']) }}

        </div>
    </div>
    <div class="panel-footer">
        <button class="btn btn-default" type="submit">
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
</div>
