@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Registro de Usuarios</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Nueva Cuenta
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-9">
                                {{ Form::open(['route' => 'registration.store', 'method' => 'POST'])}}
                                <form role="form">
                                    <div class="form-group input-group">
                                        <span class="input-group-addon">Nombre</span>
                                        {{ Form::text('fname',
                                                null,
                                                [
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Nombre',
                                                    'required' => 'required'
                                                ])
                                        }}
                                        {{ $errors->first('fname', '<span class="text-danger">:message</span>') }}
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon">Apellido</span>
                                        {{ Form::text('lname',
                                                null,
                                                [
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Apellido',
                                                    'required' => 'required'
                                                ])
                                        }}
                                        {{ $errors->first('lname', '<span class="text-danger">:message</span>') }}
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon">Correo&nbsp;</span>
                                        {{ Form::email('email',
                                                null,
                                                [
                                                    'class' => 'form-control',
                                                    'placeholder' => 'user@server.domain',
                                                    'required' => 'required'
                                                ])
                                        }}
                                        {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon">Password</span>
                                        {{ Form::password('password',
                                                [
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Password',
                                                    'required' => 'required'
                                                ])
                                        }}
                                        {{ $errors->first('password', '<span class="text-danger">:message</span>') }}
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon">Confirmar Password</span>
                                        {{ Form::password('password_confirmation',
                                                [
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Una vez mas Password',
                                                    'required' => 'required'
                                                ])
                                        }}
                                    </div>
                                    <input type="submit" class="btn btn-info btn-medium pull-right" value="Registrar">
                                </form>
                                {{ Form::close()}}
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@stop