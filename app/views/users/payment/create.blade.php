@if( isset($card) )
    {{ Form::model($card, [
        'route' => ['users.profile.card.update', $card->id],
        'role' => 'form',
        'method' => 'put'
        ]) }}

    @include('users.payment.fields')

    {{ Form::close() }}
@else
    {{ Form::open([
        'route' => 'users.profile.card.store',
        'role' => 'form',
        'method' => 'post'
        ]) }}

    @include('users.payment.fields')

    {{ Form::close() }}
@endif