<div class="panel panel-red">
    <div class="panel-heading">
        Tarjeta de Crédito

        <div class="btn-group pull-right">
            <a href="{{ route('users.profile.card.create') }}" class="btn btn-default btn-xs" >
                <i class="fa fa-plus"></i> Agregar Tarjeta de Crédito
            </a>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover" id="card">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Identificador</th>
                    <th>Código</th>
                    <th>Expiración</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @forelse($cards as $card)
                    <tr>
                        <td>{{ $card->id }}</td>
                        <td>{{ $card->identifier }}</td>
                        <td>{{ $card->last_four }}</td>
                        <td>{{ $card->expiraton }}</td>
                        <td>
                            <div>
                                <a href="#" data-toggle="modal" data-target="#confirm">
                                    <i data-method="post" data-uri="{{ route('users.profile.card.destroy', $card->id) }}" class="fa fa-trash fa-2x"></i>
                                </a>
                                <a href="{{ route('users.profile.card.edit', $card->id) }}">
                                    <i class="fa fa-pencil fa-2x"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        No hay data
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>

</div>