<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="form-group">
            <label>Nombre</label>
            {{ Form::text('first_name', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="form-group">
            <label>Apellido</label>
            {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>
<input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="form-group">
            <label>Código</label>
            {{ Form::text('last_four', null, [
                'class' => 'form-control',
                'id' => 'card-code',
                'data-url' => route('card.get-type', ':code')
              ]) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="form-group">
            <label>Identificador</label>
            {{ Form::text('identifier', null, ['class' => 'form-control']) }}
            <p class="help-block">
                Introduzca una palabra que le ayuda a identificar esta tarjeta en su lista.
            </p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <label>Fecha de Expiración</label>
        <div class="row">
            <div class="col-xs-4 col-sm-6">
                <div class="form-group">
                    <select class="selectpicker" name="expiration_month" title="Selecione un mes..." data-width="auto">
                        @foreach(Config::get('profile.months') as $id => $month)
                            <option {{ (isset($card)) && ($card->expiration_month == $id) ? 'selected' : ''}}
                                    value="{{ $id }}">{{ $month }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="selectpicker" name="expiration_year" title="Selecione un año..." data-width="auto">
                        @foreach(Config::get('profile.years') as $id => $year)
                            <option {{ (isset($card)) && ($card->expiration_year == $id) ? 'selected' : ''}}
                                    value="{{ $id }}">{{ $year }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-5">
            <div class="form-group">
                <label>Código de Seguridad</label>
                {{ Form::text('security_code', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div class="form-group">
                <label>Tipo</label>
                {{ Form::text('type', null, [
                     'class' => 'form-control',
                     'id' => 'card-type'
                     ])
                 }}
            </div>
        </div>
    </div>
</div>