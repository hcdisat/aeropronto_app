<script>

    $(document).ready(function(){
        $('#card-code').on('change', function(){
            var aero = new AeroprontoAsync();

            var options = {
                url: $(this).data('url').replace(':code', $(this).val()),
                type: 'GET'
            };

            aero.asyncRequest(options, function(response){
                $('#card-type').val(response.data);
            });
        });
    });

</script>