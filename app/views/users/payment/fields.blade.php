<div class="panel panel-red">
    <div class="panel-heading">
        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs">
            <i class="fa fa-arrow-left"></i> Atras
        </a>
        <span class="pull-right">Agregar Tarjeta de Crédito</span>
    </div>
    <div class="panel-body">
        @include('users.payment.add-card')
    </div>

    <div class="panel-footer">
        <button class="btn btn-default" type="submit" >
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
</div>