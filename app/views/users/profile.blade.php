@extends('layouts.main')


@section('page-title')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Perfil de usuario</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @stop
        @section('content')
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Información de usuario
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    @include('users.addresses.index')
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-xs-12 col-md-5">
                                    @include('users.phones.index')
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('users.payment.index')
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@stop

@section('js')
   @include('components.delete-confirm-modal')
@stop