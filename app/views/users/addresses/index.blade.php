@section('page-title')

@stop

<div class="panel panel-primary">
    <div class="panel-heading">
        Direcciónes
        <div class="btn-group pull-right">
            <a href="{{ route('users.profile.address.create') }}" class="btn btn-default btn-xs">
                <i class="fa fa-plus"></i> Agregar Dirección
            </a>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Calle</th>
                    <th>Número</th>
                    <th>Provincia</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @forelse($addresses as $address)
                    <tr>
                        <td>{{ $address->id }}</td>
                        <td>{{ $address->streat }}</td>
                        <td>{{ $address->number }}</td>
                        <td>{{ $address->city->name }}</td>
                        <td>
                            <div>
                                <a href="#" data-toggle="modal" data-target="#confirm">
                                    <i data-method="post" data-uri="{{ route('users.profile.address.destroy', $address->id) }}" class="fa fa-trash fa-2x"></i>
                                </a>
                                <a href="{{ route('users.profile.address.edit', $address->id) }}"><i class="fa fa-pencil fa-2x"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        No hay data
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
</div>