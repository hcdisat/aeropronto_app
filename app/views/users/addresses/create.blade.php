@if( isset($address) )
    {{ Form::model($address, [
        'route' => ['users.profile.address.update', $address->id],
        'role' => 'form',
        'method' => 'put'
        ]) }}

    @include('users.addresses.fields')

    {{ Form::close() }}
@else
    {{ Form::open([
        'route' => 'users.profile.address.store',
        'role' => 'form',
        'method' => 'post'
        ]) }}

    @include('users.addresses.fields')

    {{ Form::close() }}
@endif