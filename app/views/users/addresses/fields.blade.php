<div class="panel panel-primary">
    <div class="panel-heading">
        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs">
            <i class="fa fa-arrow-left"></i> Atras
        </a>
        <span class="pull-right">Agregar Dirección</span>
    </div>
    <div class="panel-body">

        <div class="form-group">
            <label>Calle</label>
            {{ Form::text('streat', null, ['class' => 'form-control']) }}
            <p class="help-block">Avenida 27 de febrero.</p>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label>Número</label>
                    {{ Form::text('number', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
            <div class="col-xs-12 col-sm-5 col-md-4">
                <div class="form-group">
                    <label>Provincia</label>
                    <select required class="selectpicker" name="city_id">
                        @foreach($cities as $id => $city)
                            <option
                                    {{(isset($address->city->id)) && ($address->city->id == $id) ? 'selected' : '' }}
                                    value="{{ $id }}">
                                {{ $city }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button class="btn btn-default" type="submit" >
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
</div>