<div class="panel panel-success">
    <div class="panel-heading">
        Teléfonos

        <div class="btn-group pull-right">
            <a href="{{ route('users.profile.phone.create') }}" class="btn btn-default btn-xs" >
                <i class="fa fa-plus"></i> Agregar Teléfono
            </a>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover" id="phone">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Número</th>
                    <th>Typo</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @forelse($phones as $phone)
                    <tr>
                        <td>{{ $phone->id }}</td>
                        <td>{{ $phone->number }}</td>
                        <td>{{ $phone->type->name }}</td>
                        <td>
                            <div>
                                <a href="#" data-toggle="modal" data-target="#confirm">
                                    <i data-method="post" data-uri="{{ route('users.profile.phone.destroy', $phone->id) }}" class="fa fa-trash fa-2x"></i>
                                </a>
                                <a href="{{ route('users.profile.phone.edit', $phone->id) }}">
                                    <i class="fa fa-pencil fa-2x"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        No hay data
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>

</div>