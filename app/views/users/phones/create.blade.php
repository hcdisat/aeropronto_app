@if(isset($phone))

    {{ Form::model($phone, [
        'route' => ['users.profile.phone.update', $phone->id],
        'role' => 'form',
        'method' => 'put'
        ]) }}

        @include('users.phones.fields')
    {{ Form::close() }}

@else

    {{ Form::open(['route' => 'users.profile.phone.store', 'role' => 'form']) }}
        @include('users.phones.fields')
    {{ Form::close() }}

@endif

