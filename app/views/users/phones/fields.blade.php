<div class="panel panel-primary">
    <div class="panel-heading">
        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs">
            <i class="fa fa-arrow-left"></i> Atras
        </a>
        <span class="pull-right">Agregar Teléfono</span>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label>Número</label>
            {{ Form::text('number', null, ['class' => 'form-control']) }}

        </div>
        {{ Form::hidden('user_id', Auth::user()->id) }}
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-4">
                <div class="form-group">
                    <label>Tipo</label>
                    <select class="selectpicker" title="Selecione un Opción..." name="phone_type_id">
                        @foreach($types as $id => $type)
                            <option {{ (isset($phone->type->id)) &&  ($id == $phone->type->id) ? 'selected' : ''}}
                                    value="{{ $id }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button class="btn btn-default" type="submit">
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
</div>