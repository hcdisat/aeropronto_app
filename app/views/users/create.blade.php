@extends('layouts.main')

@section('content')

    {{--Address--}}
    @if( Request::is('*/address/*') )
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    @include('users.addresses.create')
                </div>
            </div>
        </div>
    @endif

    {{--Phone--}}
    @if( Request::is('*/phone/*') )
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    @include('users.phones.create')
                </div>
            </div>
        </div>
    @endif

    {{--Cards--}}
    @if( Request::is('*/card/*') )
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    @include('users.payment.create')
                </div>
            </div>
        </div>
    @endif
@stop


@section('js')
    @include('users.payment.get-card-request')
@stop