@if( Auth::check() )
    @if( Auth::user()->hasRole('admin') ||  Auth::user()->hasRole('admin') )
        @include('layouts.partials.admin-sidebar')
    @else
        @include('components.sidebar')
    @endif
@else
    @include('components.pages-links')
@endif