<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            @if(Auth::user()->inRoles(['admin', 'owner']))
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-user"></i> Administracion
                                </button>
                            </span>

                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="{{ url('/package') }}"><i class="fa fa-gift  fa-fw"></i> Paquetes</a>
                </li>
                <li>
                    <a href="{{route('admin.tickets.index')}}"><i class="fa fa-info fa-fw"></i> Reclamaciones</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-database fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ route('tickets') }}">Reclamaciones</a>
                        </li>
                        <li>
                            <a href="#">Paquetes <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="{{ route('packages-billed') }}">Factuados</a>
                                </li>
                                <li>
                                    <a href="{{ route('packages-all') }}">Todos</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                @if(Auth::user()->hasRole('owner'))
                    <li>
                        <a href="{{ route('user.index') }}"><i class="fa fa-users fa-fw"></i> Usuarios</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-picture-o fa-fw"></i> Slider<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                {{link_to_route('settings.slider.index', 'Listado')}}
                            </li>
                            <li>
                                {{link_to_route('settings.slider.create', 'Crear')}}
                            </li>
                            <li>
                                {{link_to_route('settings.slider.reactivate', 'Reactivar')}}
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                @endif
                @yield('links')
                <li><hr/></li>
                <li>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Precio/Libra
                        </div>
                        <div class="panel-body">
                            <h1 class="text-muted">$RD
                                {{HTML::payment(Configuration::getPricePerPound()->value)}}
                            </h1>
                        </div>
                        <div class="panel-footer">
                            Precio/Libra
                        </div>
                    </div>
                </li>
                @yield('sub-menu')
            @endif
        </ul>

    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->