<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="{{ url('/') }}">
            <img src="{{ asset('img/page/Logo2.png') }}" alt="&copy; AEROPRONTO" class="logo">
        </a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        @if(Auth::guest())
            <li>
                <a href="{{url('login')}}">login
                    <i class="fa fa-user fa-fw"></i>
                </a>
            </li>
            <li>
                <a href="{{url('/register')}}">Register
                    <i class="fa fa-sign-in fa-fw"></i>
                </a>
            </li>
            @else
                    <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>
                    {{Auth::user()->fname}} {{Auth::user()->lname}}
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    </li>
                    @if(Auth::user()->hasRole('owner'))
                        <li>
                            <a href="{{route('settings.index')}}"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                    @endif
                    <li><a href="{{url('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                    <li>
                        <a href="{{route('users.profile')}}"><i class="fa fa-gear fa-fw"></i> Opciones de Usuario</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        @endif()
    </ul>
    <!-- /.navbar-top-links -->
   @include('layouts.partials.sidebar')

</nav>