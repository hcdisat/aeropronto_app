<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@section('title')Aeropronto @show</title>

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    {{ HTML::style('../bower_components/bootstrap/dist/css/bootstrap.min.css') }}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

    <!-- MetisMenu CSS -->
    {{ HTML::style('../bower_components/metisMenu/dist/metisMenu.min.css') }}    

    <!-- Custom CSS -->
    {{ HTML::style('../dist/css/sb-admin-2.css') }}    
    {{ HTML::style('../dist/css/aeropronto.css') }}

    <!-- Custom Fonts -->
    {{ HTML::style('../bower_components/font-awesome/css/font-awesome.min.css') }} 
    @yield('css')   

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper" class="background">
       @yield('nav')       
       @yield('body')
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    {{ HTML::script('../bower_components/jquery/dist/jquery.min.js') }}

    <!-- Bootstrap Core JavaScript -->
    {{ HTML::script('../bower_components/bootstrap/dist/js/bootstrap.min.js') }}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    {{ HTML::script('../bower_components/metisMenu/dist/metisMenu.min.js') }}    
    {{ HTML::script('js/bootstrap-typeahead.js') }}

    <!-- Custom Theme JavaScript -->
    {{ HTML::script('../dist/js/sb-admin-2.js') }}    
    {{ HTML::script('js/init.js') }}
    {{ HTML::script('js/aeropronto.js') }}
    {{ HTML::script('js/modalmanager.js') }}
    @yield('js')
</body>

</html>