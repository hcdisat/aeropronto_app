@extends('layouts.master')

@section('nav')
    @include('layouts.partials.navbar')
@stop
@section('body')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-6 pull-right">
                @include('components.notifications')
            </div>
        </div>
        <div class="row">
            <br/>
            @yield('page-title')
        </div>
        @yield('content')
    </div>
@stop 