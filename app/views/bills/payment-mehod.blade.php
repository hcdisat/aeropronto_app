@extends('layouts.main')

@section('page-title')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Forma de Pago / Forma de Entrega</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @stop
        @section('content')
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(['route' => 'bill.payonline', 'method' => 'post']) }}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <h4>Elija Forma de envio</h4>
                                    <select class="selectpicker" name="deliver_id" data-width="auto">
                                        <option value="2">Recoger en sucursal</option>
                                        <option value="1">Envio a domicilio</option>
                                    </select>
                                </h4>
                                <hr>
                            </div>
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Seleccione una de sus tarjetas</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <select class="selectpicker" name="cardNumber" title="Tarjetas..." data-width="auto">
                                                @foreach($cards as $id => $card)
                                                    <option value="{{ $id }}">{{ $card }}</option>
                                                @endforeach
                                                <?php unset($card); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Usar otra Tarjeta</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            @include('users.payment.add-card')
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Elija Forma de envio</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-success" type="submit">
                                <i class="fa fa-money"></i> Continuar
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    @include('users.payment.get-card-request')
@stop