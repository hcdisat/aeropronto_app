@extends('layouts.main')

@section('css')
@include('components.slider-css')
@stop

@section('page-title')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mis Paquetes</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @stop
        @section('content')
            <div class="row">
                <div class="col-lg-12">
                    @if( !$packages->isEmpty() )
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            Listado
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <table class="table table-striped table-hover dt-responsive" width="100%" cellspacing="0">
                                        <tr>
                                            <th>Tracking</th>
                                            <th>Estado</th>
                                            <th>Peso</th>
                                            <th>Cant de articulos</th>
                                            <th>Pago</th>
                                            <th>Acciones</th>
                                        </tr>
                                        @foreach($packages as $package)
                                            <tr>
                                                <td><i class="pointer fa fa-plus-square-o"></i> {{ $package->tracking }}</td>
                                                <td>{{ $package->status->description }}</td>
                                                <td role="weight">
                                                    {{ HTML::totalWeight($package->items->sum('weight')) }} LBS
                                                </td>
                                                <td>{{ $package->items->count()}}
                                                    {{ $package->items->count() == 1
                                                        ? 'Item'
                                                        : 'Items'
                                                    }}
                                                </td>
                                                <td >$RD {{ HTML::payment($package->items->sum('price'))}}
                                                </td>

                                                <td>
                                                    <div>
                                                        @if( $package->hasStatus(7) )
                                                            <a class="btn btn-warning" disabled="disabled">
                                                                <i class="fa fa-credit-card "></i> Pagado
                                                            </a>
                                                        @else
                                                            <a class="btn btn-success" href="{{ url('users/packages/payment/'.$package->id) }}">
                                                                <i class="fa fa-money "></i> Pagar online
                                                            </a>
                                                        @endif
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                    </div>
                    @endif
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@stop