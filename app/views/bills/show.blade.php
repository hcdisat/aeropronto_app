@extends('components.reports.main')
@section('report')
<div class="report-wrapper">
	<div class="container-fluid">
		<div class="col col-lg-12">
			<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-sm-6">
						<p>Factura: F-PSDQ-{{$package_id}}</p>
						<p>Fecha: {{$now}}</p>
					</div>
					<div class="col-sm-6">
						<p>Nombre: {{$bill->user->lname}}, {{$bill->user->fname}}</p>
						<p>Cuenta: CLI-SDQ-{{$package->user->id}}</p>
					</div>
				</div>            
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<h3>Estado del Paquete: Facturado }}</h3>
				<table class="table table-striped table-bordered table-hover items" id="itemsTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Tracking no.</th>
							<th>Nombre</th>
							<th>Peso</th>
							<th>Cobro</th>							
						</tr>
					</thead>
					<tbody>    
						@foreach($bill->billLines->package->items as $item)
						<tr id="{{'item-row-'.$item->id}}">  
							<td>{{ $package->tracking}}</td>
							<td>{{ $item->name}}</td>
							<td role="weight">{{ HTML::totalWeight($item->weight) }} LBS</td>
							<td role="price">$RD {{ HTML::payment($item->price) }}</td>							
						</tr>
						@endforeach
					</tbody>
				</table>      				
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
			<div class="panel-footer">
            <div class="row">                         
              <div class="col-md-4 pull-right text-right">
                <span>Cobro total</span>
                <h4 id="total-price">$RD{{HTML::payment($total)}}</h4>
              </div>
               <div class="col-md-4 pull-right text-right">
                <span>Peso total</span>
                <h4 id="total-weight">{{HTML::totalWeight($weight)}} LBS</h4>
              </div>
            </div>
          </div>
      </div>
      <!-- /.panel -->     
		</div>
  </div>
</div>
@stop