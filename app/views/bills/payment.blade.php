@extends('layouts.main')

@section('page-title')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Pagos Online</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @stop
        @section('content')
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Paquete {{ $package->tracking }}
                            {{ Form::open(['route' => 'bill.payment', 'method' => 'post', 'class' => 'form-inline clearfix']) }}
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-success btn-xs">
                                    <i class="fa fa-money "></i> Continuar
                                    <input type="hidden" value="{{ $package->id }}" name="package_id">
                                </button>
                            </div>
                            {{ Form::close() }}

                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <h2>Articulos</h2>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <table class="table table-striped table-hover dt-responsive" width="100%" cellspacing="0">
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Descripcion</th>
                                                    <th>Peso</th>
                                                    <th>Precio</th>
                                                </tr>
                                                @foreach($package->items as $item)
                                                    <tr>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->description }}</td>
                                                        <td role="weight">
                                                            {{ HTML::totalWeight($item->weight) }} LBS
                                                        </td>
                                                        <td >$RD {{ HTML::payment($item->price)}}</td>
                                                    </tr>
                                                @endforeach

                                            </table>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <hr>
                                    <!-- /.row (nested) -->
                                    <table class="table table-striped table-hover dt-responsive" width="100%" cellspacing="0">
                                        <tr>
                                            <th>Cant de articulos</th>
                                            <th>Fecha</th>
                                            <th>Peso</th>
                                            <th>Pago</th>
                                        </tr>

                                        <tr>
                                            <td>{{ $package->items->count()}}
                                                {{ $package->items->count() == 1
                                                    ? 'Articulo'
                                                    : 'Articulos'
                                                }}
                                            </td>
                                            <td>{{ $package->created_at->format('l jS \\of F Y h:i:s A') }}</td>
                                            <td role="weight">
                                                {{ HTML::totalWeight($package->items->sum('weight')) }} LBS
                                            </td>
                                            <td >$RD {{ HTML::payment($package->items->sum('price'))}}
                                            </td>


                                        </tr>
                                    </table>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->

                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@stop