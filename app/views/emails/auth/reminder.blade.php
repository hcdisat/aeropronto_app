<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Restablecer Contraseña</h2>

		<div>
			para restablecer su contraseña, complete este formulario: {{ URL::to('password/reset', array($token)) }}.<br/>
			este link expirará en {{ Config::get('auth.reminder.expire', 60) }} minutos.
		</div>
	</body>
</html>
