<h2>Saludos Sr/Sra {{$package->user->getFullName()}}</h2>
<h2>Paquete: {{$package->status->name}}</h2>

<p>Su Paquete está disponible para facturar y retirar en nuestra sucursal. </p>
<p>	Puede pagar en linea haciendo click en este <a href="{{ url('users/packages/payment/'.$package->id) }}">link</a>.</p>
<small> nota: Tiene que estar logeado</small>
 <p>Saludos!.</p>

 <div>Tracking No.</div>
<div>{{ $package->tracking }}</div>

@if($package->items->count() > 0)	
	<ul>
    	@foreach($package->items as $item)
      	<li>Articulo: {{$item->name}}</li>
      	<li>Peso: {{$item->weight}} LIBS</li>
      	<li>Precio: $RD {{$item->price}}</li>
      	<hr />
    	@endforeach
    </ul>
@endif