{{ Form::open(['route' => 'tickets.store', 'method' => 'post', 'id' => 'store-ticket']) }}
    @include('helpdesk.partials.ticket-fields', ['packages' => $packages])
{{ Form::close() }}