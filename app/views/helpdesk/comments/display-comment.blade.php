<li class=":handed clearfix">
    <span class="chat-img pull-:handed">
        <img src="{{ asset('img/chat/:pic.png') }}" alt="User Avatar" class="img-circle" />
    </span>
    <div class="chat-body clearfix">
        <div class="header">
            @if( !$comment->user->inRoles(['owner', 'service', 'admin']))
                <strong class="primary-font">{{ $comment->user->getFullname() }}</strong>
                <small class="pull-right text-muted">
                    <i class="fa fa-clock-o fa-fw"></i> {{ $comment->created_at->diffForHumans() }}
                </small>
             @else
                <small class="text-muted">
                    <i class="fa fa-clock-o fa-fw"></i> {{ $comment->created_at->diffForHumans() }}
                </small>
                <strong class="pull-right primary-font">{{ $comment->user->getFullname() }}</strong>
            @endif
        </div>
        <p>
            {{ trim($comment->comment) }}
        </p>
    </div>
</li>