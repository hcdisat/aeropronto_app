@extends('layouts.main')

@section('css')
    @include('components.table-prsentation-tickets')
@stop


@section('sub-menu')
    <li><hr/></li>
    <li>
        <div class="panel panel-default">
            <div class="panel-heading">
                Filtros
            </div>
            <div class="panel-body">
                <ul class="nav" id="tickets-submenus">
                    <li>
                        <a href="{{route('admin.tickets.get')}}"><i class="fa fa-circle-o fa-fw"></i> Todas las Reclamacioes</a>
                    </li>
                    <li>
                        <a href="{{route('admin.tickets.get')}}?mine=true"><i class="fa fa-circle-o fa-fw"></i> Mis Reclamacioes</a>
                    </li>
                    <li>
                        <a href="{{route('admin.tickets.get')}}?asigned=2"><i class="fa fa-circle-o fa-fw"></i> Reclamacioes Asignadas</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
@stop

@section('content')
@section('page-title')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Administración de Reclamaciones</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @stop

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Reclamaciones - Panel de Control

                        <div class="btn-group pull-right">
                            <button type="button" id="btn-add" class="btn btn-default btn-xs" data-toggle="modal" data-target="#add-ticket">
                                <i class="fa fa-plus"></i> Agregar Reclamación
                            </button>
                        </div>

                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table id="ticketsTable" class="table table-striped table-hover dt-responsive" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Estado</th>
                                <th>Prioridad</th>
                                <th>Asignado?</th>
                                <th>Asunto</th>
                                <th>Fecha</th>
                                <th>Paqute Asociado</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                    <!--div class="panel-footer">
                </div-->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
        </div>

    </div>
    </div>
@stop

@section('js')
    {{ HTML::script('js/tickets.js') }}
    {{ HTML::script('bower_components/bootstrap-select/js/bootstrap-select.js') }}
    {{ HTML::script('bower_components/datatables/media/js/jquery.dataTables.js') }}
    {{ HTML::script('bower_components/datatables-responsive/js/dataTables.responsive.js') }}
    {{ HTML::script('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}

    <script>

        $(document).ready(function(){

            $.validate({
                form : '#store-ticket'
            });

            // tickets Control
            var ticketWorker = new TicketsManager('#ticketsTable', 'add-ticket', '/admin/tickets');

            ticketWorker.getClientTableAction();

            $('#tickets-submenus').on('click', 'li', ticketWorker.setActiveAction);

            $('#tickets-submenus').on('click', 'a', ticketWorker.getDataAction);

            $('tbody').on('click', 'button.action', ticketWorker.openTicketAction);

            ticketWorker.openAddTicketAction();


            $('.modal-footer .btn.btn-primary').click(ticketWorker.storeTicketAction);


        });

    </script>
@stop