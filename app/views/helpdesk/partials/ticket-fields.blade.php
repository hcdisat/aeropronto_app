<div class="panel panel-info">
    <div class="panel-heading">
        @if( Auth::user()->inRoles(['service', 'admin']) )
            <div class="form-group">
                <label>Tracking del paquete</label>
                @include('components.packages.package-finder')
                <p id="user-name" class="label label-info" hidden></p>
            </div>
        @endif
    </div>
    <div class="panel-body">
        <div class="form-group">
            @if( !Auth::user()->inRoles(['service', 'admin', 'owner']) && !empty($packages))
                <label>Seleccione un paquete</label><br>
                <select class="selectpicker" name="package_id" id="package_id"
                        data-url="/package/" data-method="PUT" data-package="{{ Input::old('package_id') }}">
                    @foreach($packages as $id => $package)
                        <option value="{{$id}}" {{(isset($ticket->package->id)) && ($id == $ticket->package->id) ? 'selected="selected"': ''}}>
                            {{$package}}
                        </option>
                    @endforeach
                </select>
            @endif
        </div>

        <div class="form-group">
            <label>Selecione un Asunto</label><br>
            <select class="selectpicker" name="subject_id" id="subject_id"
                    data-package="{{ Input::old('subject_id') }}">
                @foreach($subjects as $id => $subject)
                    <option value="{{$id}}">
                        {{$subject}}
                    </option>
                @endforeach
                    <option value="0">Otro</option>
            </select>
            <hr>
            {{ Form::text('subject', Input::old('subject'), [
                'class' => 'form-control hidden',
                'data-validation' => 'required',
                'id' => 'subject',
                'placeholder' => 'Ingrese el asunto'
                ])
             }}
            <p class="help-block">Asunto o tema de la reclamación</p>
        </div>

        <div class="form-group">
            <label>Descripción</label>
            {{
                Form::textarea('description', Input::old('description'), [
                    'class' => 'form-control',
                    'rows' => 4,
                    'data-validation' => 'required'
                ])
             }}
        </div>
    </div>
</div>

<script>
    $(function(){
        // Initialize ajax autocomplete:
        $('#find-package').typeahead({
            ajax: '/packages/find/',
            onSelect: showForm
        });

        $('.selectpicker').selectpicker({
            style: 'btn-info',
            size: 4
        });
    });

    function showForm(data)
    {
        $('#package_id').val(data.value);

        var handler = function(name)
        {
            $('#user-name').html(name);
            $('#user-name').show();
        };

        var aero = new AeroprontoAsync();
        aero.asyncRequest({
            url: '/packages/get/username/'+data.value
        }, handler);

    }

    $('#subject_id').change(function(){
        if( $(this).selectpicker('val') == 0 )
        {
            $('#subject').removeClass('hidden');
        }
        else {
            $('#subject').addClass('hidden');
        }
    });

</script>