@extends('layouts.main')

@if( Auth::user()->inRoles(['admin', 'service']) )
@section('sub-menu')
    <li>
       {{ Form::open(['route' => ['admin.tickets.update', $ticket->id], 'method' => 'PUT']) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                Acciones
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Asignar a:</label>
                    <select multiple class="selectpicker" name="user_ids[]">
                        @foreach($users as $userId => $userNme)
                            <option {{ in_array($userId , $ticket->users()->lists('user_id')) ? 'selected="selected"' : '' }} value="{{ $userId }}">{{ $userNme }}</option>
                        @endforeach
                    </select>
                </div>Use
                <div class="form-group">
                    <label>Estado</label>
                    <select class="selectpicker" name="ticket_status_id">
                        @foreach($statuses as $statusId => $statusName)
                            <option {{ $statusId == $ticket->status->id ? 'selected="selected"' : '' }} value="{{ $statusId }}">
                                {{ $statusName }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label> Prioridad</label>
                    <select class="selectpicker" name="ticket_priority_id">
                        @foreach($priorities as $priorityId => $priority)
                            <option {{ $priorityId == $ticket->priority->id ? 'selected="selected"' : '' }} value="{{ $priorityId }}">
                                {{ $priority }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-save"></i> Guardar Cambios
                </button>
            </div>
        </div>
        {{ Form::close() }}
    </li>
@stop
@endif

@section('content')
@section('page-title')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <a href="{{ URL::previous() }}" class="btn btn-primary btn-xl">
                        <i class="fa fa-arrow-circle-left fa-2x"></i>
                         Atras
                    </a>
                    Reclamación # {{ $ticket->id }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @stop

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Estado: {{ HTML::labelStatus($ticket->status) }} |
                        Prioridad: {{ HTML::labelPrioriry($ticket->priority) }} |
                        Aisignado?: {{ HTML::labelYesOrNo((bool)$ticket->is_asigned) }}

                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">{{ $ticket->subject }}</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-col">
                                <div class="panel-body">
                                    {{ $ticket->description }}

                                </div>
                            </div>
                            <div class="panel-footer">
                                <p>Cliente: <span class="badge">{{ $ticket->user->full_name }}</span></p>
                                <p>Email: <span class="badge">{{ $ticket->user->email }}</span></p>
                                @if(isset($ticket->package))
                                    <p>Paquete:  <span class="label label-info">{{ $ticket->package->tracking }}</span></p>
                                @endif

                            </div>
                        </div>
                    </div>
                    @if( $ticket->is_asigned )
                    <div class="panel-footer">
                        <span class="label label-primary">Aisignado a:</span>

                        <ul class="pull-right">
                            @foreach($ticket->users as $user)
                                <li class="badge">{{ $user->full_name }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- /.panel-body -->
                    <!--div class="panel-footer">
                </div-->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
        </div>


        <div class="row">
            <div class="col-xs-12">
                <div class="chat-panel panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-comments fa-fw"></i>
                        Comentarios
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <ul class="chat" id="comments-list">
                            @forelse($ticket->comments->sortBy('id') as $comment)
                                {{ HTML::showComment($comment) }}
                            @empty
                                <li class="left clearfix">
                                    No se han agregado comentarios en este reclamación.
                                </li>
                            @endforelse

                        </ul>
                    </div>
                    <!-- /.panel-body -->
                    @if( $ticket->is_asigned )
                        <div class="panel-footer">
                            {{ Form::open(['route' => 'comments', 'method'=> 'post', 'id' => 'post-comment']) }}
                            <div class="input-group">
                                <input id="btn-input" name="comment" type="text" class="form-control input-sm" placeholder="Escriba su commentrio aqui..." />
                                <input name="ticket_id" type="hidden" value="{{ $ticket->id }}">
                                <input name="user_id" type="hidden" value="{{ Auth::user()->id }}">
                                <span class="input-group-btn">
                                    <button class="btn btn-warning btn-sm" id="send-comment" type="submit">
                                        Send
                                    </button>
                                </span>
                            </div>
                            {{ Form::close() }}
                        </div>
                    @endif
                    <!-- /.panel-footer -->
                </div>
                <!-- /.panel .chat-panel -->
            </div>
        </div>
    </div>
@stop

@section('js')
    
    <script>

        $('#send-comment').click(function(event){

            event.preventDefault();
            var aero = new AeroprontoAsync();

            var $form = $('form#post-comment');

            var options = {
                url: $form.attr('action'),
                type: $form.attr('method'),
                data: $form.serialize()
            };

            var onSuccess = function(response) {

                $('#comments-list').append($(response.data));
                $('#btn-input').val('');
            };

            aero.asyncRequest(options, onSuccess);

        });
    </script>
@stop
