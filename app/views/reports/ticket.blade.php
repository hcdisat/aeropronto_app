<div class="page-header">
    {{ !isset($from) ?: 'Desde '.$from }}
    {{ !isset($to) ?: 'Hasta '.$to }}
</div>
<hr>
<div class="table-responsive">
    <table class="table table-hover" id="card">
        <thead>
        <tr>
            <th>#</th>
            <th>Cliente</th>
            <th>Estado</th>
            <th>Prioridad</th>
            <th>Creado en</th>
            <th>Aisgnado</th>
        </tr>
        </thead>
        <tbody>
        @forelse($report as $ticket)
            <tr>
                <td>{{ $ticket->id }}</td>
                <td>{{ $ticket->user->full_name }}</td>
                <td>{{ $ticket->status->name }}</td>
                <td>{{ $ticket->priority->name }}</td>
                <td>{{ $ticket->created_at->toDateString() }}</td>
                <td>
                    {{ $ticket->is_asigned ? 'Si' : 'No' }}
                    <ul>
                        @if( $ticket->is_asigned )
                            <li>Asignado a</li>
                            @foreach($ticket->users as $user)
                                <li>{{ $user->full_name }}</li>
                            @endforeach
                        @endif
                    </ul>
            </tr>
        @empty
            <tr>
                No hay data
            </tr>
        @endforelse
        </tbody>
    </table>
</div>