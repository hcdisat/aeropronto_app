<div class="panel panel-default">
    <div class="panel-heading">
        {{ !isset($from) ?: 'Desde '.$from }}
        {{ !isset($to) ?: 'Hasta '.$to }}
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover" id="card">
                <thead>
                <tr>
                    <th>Tracking</th>
                    <th>Cliente</th>
                    <th>Estado</th>
                    <th>Fecha</th>
                </tr>
                </thead>
                <tbody>
                @forelse($report as $package)
                    <tr>
                        <td>{{ $package->tracking }}</td>
                        <td>{{ $package->client }}</td>
                        <td>{{ $package->estado }}</td>
                        <td>{{ $package->created_at }}</td>
                    </tr>
                @empty
                    <tr>
                        No hay data
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel-footer">


    </div>

    <!--div class="panel-footer">
</div-->
</div>