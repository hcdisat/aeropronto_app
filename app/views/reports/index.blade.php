@extends('layouts.main')

@section('css')
    {{ HTML::style('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}
@stop


@section('content')
@section('page-title')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Reportes
                    <hr> <small> {{ $report_name }}</small></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @stop
        <div class="row">
            <div class="col-lg-{{ isset($report) ? '3' : '8 col-lg-offset-2' }}">
                {{ Form::open(['method' => 'post']) }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Parametros de reporte

                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class='col-md-12'>
                                <div class="form-group">
                                    <label for="datetimepicker6">Desdes</label>
                                    <div class='input-group date' id='datetimepicker6'>
                                        <input type='text' class="form-control" name="from" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-md-12'>
                                <div class="form-group">
                                    <label for="datetimepicker7">Hasta</label>
                                    <div class='input-group date' id='datetimepicker7'>
                                        <input type='text' class="form-control" name="to"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Formato del Reporte</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type"  value="html" checked>Html
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" value="pdf">PDF
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <button class="btn btn-default" type="submit">Generar</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="col-lg-9">
                @if( isset($report) )
                    @include($view)
                @endif
            </div>

        </div>
    </div>
@stop

@section('js')
    {{ HTML::script('/bower_components/moment/min/moment.min.js') }}
    {{ HTML::script('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}
    <script>
        $(function () {
            $('#datetimepicker6').datetimepicker();
            $('#datetimepicker7').datetimepicker({
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@stop