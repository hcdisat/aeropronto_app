{{Form::hidden('package_id', null, ['id' => 'package_id'])}}
<div class="form-group">
    <label>Nombre</label>
    {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required'])}}
    <p class="help-block">Ejemplo "Telefono Celular".</p>
</div>
<div class="form-group">
    <label>Peso</label>
    {{ Form::text('weight', null, ['class' => 'form-control', 'required' => 'required'])}}
    <p class="help-block">Ejemplo: en libras "5lb".</p>
</div>
<div class="form-group">
    {{ Form::hidden('price', null, ['id' => 'price'])}}
</div>
<div class="form-group">
    {{ Form::label('description', 'Decripción') }}
    {{ Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required'])}}
</div>
<div class="alert-danger hide">
    <ul id="error-display">

    </ul>
</div>