@extends('layouts.master')
@section('title')
    Login | @parent    
@stop
@section('body')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <a href="/" class="btn btn-default btn-sm">
                            <i class="fa fa-arrow-circle-left"></i> Atras
                        </a>
                        <h3 class="panel-title pull-right">Por favor inicie sesión</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open([
                            'route' => 'session.store', 'role' => 'form', 'method' => 'post']) }}                        
                            <fieldset>
                                <div class="form-group">
                                    {{ Form::email('email', null, 
                                        [
                                            'class' => 'form-control', 
                                            'placeholder' => 'E-mail', 
                                            'required' => 'required',
                                            'autofocus'
                                        ]) 
                                    }}
                                    {{ $errors->first('email', '<span class="text-danger">:message</span>') }}                                             
                                </div>                                
                                <div class="form-group">
                                    {{ Form::password('password', 
                                        [
                                            'class' => 'form-control', 
                                            'required' => 'required',
                                            'placeholder' => 'password'
                                        ]) 
                                    }}
                                    {{ $errors->first('password', '<span class="text-danger">:message</span>') }}
                                </div>                                                                                        
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        @if(Session::has('flash_message'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('flash_message') }}</p>    
                            </div>                            
                        @endif                        
                        {{ Form::close() }}
                    </div>
                    <div class="panel-footer">
                        {{link_to('password/remind', '¿Olvidó si contraseña?')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop