@extends('layouts.main')
@section('content')
@include('components.packages.show')
<div class="container">	
	<div class="row">
		<div class="col col-md-1">
			<a class="btn btn-default" href="{{route('package.index')}}">
				<i class="fa fa-arrow-left "></i> Atras
			</a>
		</div>
		@if($package->status->name === 'Disponible' && !$package->hasStatus(7))
		<div class="col col-md-1">
			<a class="btn btn-default" href="{{url('package/bill/'.$package->id)}}">
				<i class="fa fa-print "></i> Imprimir
			</a>
		</div>
		@endif
	</div>
</div>
@stop