@extends('layouts.main')

@section('page-title')
  <h1 class="page-header">Administrador de Artículos</h1>
@stop

@section('content')
  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Artículos
          </div>
          {{ Form::open(['route'=>'package.store', 'role' => 'form', 'method' => 'POST']) }}
          <div class="panel-body">
            <div class="row">

              <div class="col-md-8">
                <span>Buscar usuario por nombre</span>
                <div class="form-group input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i> </span>
                  @include('components.packages.user-finder')
                </div>
                <h2><small>Articulo</small></h2>
                <hr>
                @include('items.add')
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <div class="clearfix">
              <button type="submit" class="btn btn-primary pull-right" id="btnAddItem" data-url="items">Guardar</button>
            </div>
          </div>
          {{Form::close()}}
        </div>
      </div>

    </div>

  </div>
@stop
@section('js')
  <script>
    $(function(){
      // Initialize ajax autocomplete:
      $('#find-user').typeahead({
        ajax: '/find/',
        onSelect: showForm
      });

      /*$('#find-user').on('keyup', function(){
       if( !$(this).val()){
       $('div#hidder').addClass('hide');
       }
       });*/
    });

    function showForm(data)
    {
      $('#user_id').val(data.value);
      //$('div#hidder').removeClass('hide');
    }
  </script>
@stop