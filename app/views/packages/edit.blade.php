@extends('layouts.main')

@section('css')
  @include('components.table-prsentation')
@stop

@section('content')
  {{ Form::open(['route' => 'package.update', 'id' => 'changeStatus', 'method' => 'PUT']) }}
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Editar Paquete {{ $package->tracking }}
        <div class="clearfix">
          <button type="button" data-toggle="modal" data-target=".bs-edit-package"
                  class="btn btn-info btn-pencil pull-right" data-package="{{$package->id}}" id="edit-package">
            <i class="fa fa-th-list"></i> Ediitar Articulos
          </button>
        </div>
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <div class="col-md-6">
              <span>Estado</span>
              <br/>
              <select class="selectpicker" name="status" id="packageStatus"
                      data-url="/package/" data-method="PUT" data-package="{{$package->id}}">
                @foreach($statuses as $key => $value)
                    <option value="{{$key}}" {{$key == $package->status_id ? 'selected="selected"': ''}}>
                      {{$value}}
                    </option>
                @endforeach
              </select>
            </div>

          </div>
          <hr>
        </div>
        <hr>
        <div class="row">
          <div class="col col-lg-4">
            <p>Factura: {{$package->tracking}}</p>
            <p>Fecha: {{HTML::parseDate($package->created_ad)}}</p>
          </div>
          <div class="col col-lg-4 col-offset-4">
            <p>Nombre: {{$package->user->getFullName()}}</p>
            <p>Cuenta: CLI-SDQ-{{$package->user->id}}</p>
          </div>
        </div>
        <hr>
        {{Form::hidden('id', $package->id, ['id' => 'package_id'])}}
        {{Form::hidden('status_id', $package->status->id, ['id' => 'status_id'])}}
                <!-- /.panel-body -->
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary">Guargar</button>
        </div>
      </div>
      <!-- /.panel -->
    </div>
  </div>
  {{ Form::close() }}

  @include('components.packages.edit-package')
  @include('components.items.delete-item')
  @include('components.packages.add-article-modal')
  @include('components.items.edit-item')

@stop

@section('js')
  {{ HTML::script('bower_components/bootstrap-select/js/bootstrap-select.js') }}
  {{ HTML::script('bower_components/datatables/media/js/jquery.dataTables.js') }}
  {{ HTML::script('bower_components/bootstrap-modal/js/bootstrap-modalmanager.js') }}
  {{ HTML::script('bower_components/bootstrap-modal/js/bootstrap-modal.js') }}
  {{ HTML::script('bower_components/datatables-responsive/js/dataTables.responsive.js') }}
  {{ HTML::script('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}
@stop
