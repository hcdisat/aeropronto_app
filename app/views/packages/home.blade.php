@extends('layouts.main')
@section('css')
    @include('components.table-prsentation')
@stop
@section('page-title')
<div class="container-fluid">
    @if($packages->count() > 0)
        <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-small btn-default pull-right" href="{{ route('package.create') }}"><i class="fa fa-plus"></i> Agregar Paquete</a>
        </div>
        <hr>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Paquetes - Panel de Control
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table id="packageTable" class="table table-striped table-hover dt-responsive" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Tracking</th>
                            <th>Usuario</th>
                            <th>Status</th>
                            <th>Peso</th>
                            <th>Cant de articulos</th>
                            <th>Pago</th>
                            <th>Creado en</th>
                            <th>Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($packages as $package)
                        @include('components.packages.rows', $package)
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            <!--div class="panel-footer">
        </div-->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
    @else
        <h3>No existen Paquetes.</h3>
        <span>Agregar un <a href="{{url('/package/create')}}">paquete</a></span>
    @endif
</div>

@if( $packages->count())
@include('components.packages.edit-package', $packages)
@include('components.items.delete-item')
@include('components.packages.delete-modal')
@include('components.packages.add-article-modal')
@include('components.items.edit-item')
@endif


@stop
@section('js')
{{ HTML::script('bower_components/bootstrap-select/js/bootstrap-select.js') }}
{{ HTML::script('bower_components/datatables/media/js/jquery.dataTables.js') }}
{{ HTML::script('bower_components/bootstrap-modal/js/bootstrap-modalmanager.js') }}
{{ HTML::script('bower_components/bootstrap-modal/js/bootstrap-modal.js') }}
{{ HTML::script('bower_components/datatables-responsive/js/dataTables.responsive.js') }}
{{ HTML::script('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}
<script>
     $('#packageTable').DataTable({
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });

    $('#itemsTable_filter input').addClass('form-control input-sm');
</script>
@stop