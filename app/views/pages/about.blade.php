@extends('layouts.main')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sobre Nosotros</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Quienes Somos
				</div>
				<div class="panel-body">
					<p>{{{$we->value}}}</p>
				</div>				
			</div>
		</div>
		<!-- /.col-lg-4 -->
		<div class="col-lg-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Misión
				</div>
				<div class="panel-body">
					<p>{{{$mission->value}}}</p>
				</div>				
			</div>
		</div>
	</div>
	<div class="row">
		<!-- /.col-lg-4 -->
		<div class="col-lg-6">
			<div class="panel panel-success">
				<div class="panel-heading">
					Visión
				</div>
				<div class="panel-body">
					<p>{{{$vision->value}}}</p>
				</div>				
			</div>
		</div>
		<!-- /.col-lg-4 --><!-- /.col-lg-4 -->
		<div class="col-lg-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					Valores
				</div>
				<div class="panel-body">
					<p>{{{$values->value}}}</p>
				</div>				
			</div>
		</div>
		<!-- /.col-lg-4 -->
	</div>
</div>
<!-- /.container-fluid -->
@stop