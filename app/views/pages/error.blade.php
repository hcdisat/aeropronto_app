@extends('layouts.main')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
		<h1 class="page-header">Accesso Denegado <i class="fa fa-lock"></i></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<h5>No tiene permisos para acceder a esta area. <br>
			haga click <a href="{{URL::previous()}}">aquí </a>para regresar.</h5>
	<!-- /.row -->
</div>
<!-- /.container-fluid -->
@stop