@extends('layouts.main')
@section('css')
	@include('components.slider-css')
@stop
@section('content')
@section('page-title')
<div class="container-fluid">
<div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Aeropronto</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
@stop
<div class="row">
  <div class="col-md-3">
        <div class="panel panel-default" id="midCol">
          <div class="panel-heading"></div>
          <div class="panel-body">
            <div class="well">
              {{HTML::image('img/page/QR.png', null, ['class' => 'img-responsive'])}}
              <h3>
                  <a href="https://play.google.com/store/apps/details?id=appinventor.ai_fernandezjansel.Aeropronto">
                      descarga nuesta aplicacion.
                  </a>
              </h3>
                <!--Ofertas-->
            </div>
          </div>
        </div><!--/panel-->
      </div><!--/end mid column-->
      @include('components.slider', $sliders)
</div>
</div>
@stop
