@extends('layouts.main')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Contacto</h1>
		</div>
		<!-- /.col-lg-12 --	>
	</div>
	<!-- /.row -->
	<div class="row">
	<div class="col col-lg-2 col-offset-4"></div>
	 <div class="col col-lg-8 col-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					Como contactarnos
				</div>
				<div class="panel-body">
					<dl>						
						<dt>Dirección</dt>
						<dd>{{{$profile->address}}}</dd><br>
						<dt>Teléfonos</dt>
						<dd>{{{$profile->phone_one}}}</dd>
						<dd>{{{$profile->phone_two}}}</dd><br>
						<dt>Correo Electrónico</dt>
						<dd>{{{$profile->email}}}</dd><br>
					</dl>
				</div>
				<div class="panel-footer">
					{{{$profile->city}}}
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.container-fluid -->
@stop