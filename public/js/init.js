$(function() {

    //Packages Control
    var packageWorker = packageManager();
    var userWorker = userManager();

    $('#packageTable')
        .on('click','button.btn.btn-default.btn-circle', packageWorker.setTarget);

    $('#userTable')
        .on('click','button.btn.btn-danger.btn-circle', userWorker.prepareDelete);
    $('#del-user').on('click', userWorker.deleteAction);

    $('#packageTable')
        .on('click','button.btn.btn-success.btn-circle', packageWorker.setPackageId);

    $('#edit-package')
        .on('click', packageWorker.loader);

    $('#package-delete').on('click', packageWorker.remove);

    $('#btnAddItem').on('click', packageWorker.add);

    $('#packageTable_filter input').addClass('form-control');

    packageWorker.autoCloseNotifications();

});