var modalsActions = function() {

    var deleteRowAction = function(rowId, modalId){
        $(rowId).remove();
        $(modalId).modal('hide');
    };

    var updateOrCreateRowAction = function(rowId, tr){
        if(tr){
            $(rowId).replaceWith(tr);
        }
    };

    var closeModalAction = function(modalId) {
        var showErrors = $('.alert-danger');

        showErrors.each(function(){
            $(this).addClass('hide');
            $('#item-error-display').html('');
        });

        $(modalId).modal('hide');
    };

    return {
        deleteRow: deleteRowAction,
        updateOrCreateRow: updateOrCreateRowAction,
        closeModal: closeModalAction
    };
};

var modalModel = function() {

    var _modalId;
    var _title;
    var _action;

    var construct = function(modalId, title, fnAction) {
        _modalId = modalId;
        _title = title;
        _action = fnAction;
        configureModal();
        return getModal();
    };

    var getModal = function() {
        if(_modalId){
            return _modalId.contains('#')
                ? $(_modalId)
                : $('#'+ _modalId);
        }
    }

    var configureModal = function() {
        var modal = getModal();
        modal.find('#myModalLabel').val(_title);
        var action = modal.find('button').last();
        action.unbind('click');
        action.on('click', _action);
    };

    return {
        modalModel: construct
    };

};