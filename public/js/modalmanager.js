var Modal = function(id)
{
    var modalLabel = id + 'Label';
    var modal = $('<div>').addClass('modal fade')
        .attr('id', id)
        .attr('tabindex', -1)
        .attr('role', 'dialog')
        .attr('aria-labelledby', modalLabel)
        .attr('aria-hidden', true);

    var modalDialog = $('<div>').addClass('modal-dialog');
    var modalContent = $('<div>').addClass('modal-content');
    var modalHeader = $('<div>').addClass('modal-header');

    var closeButton = $('<button>').addClass('close')
        .attr('type', 'button')
        .attr('data-dismiss', 'modal')
        .attr('aria-hidden', true);

    var times = $('<i>').addClass('fa fa-times');

    var modalTitle = $('<h4>').addClass('modal-title')
        .attr('id', modalLabel);

    var modalBody = $('<div>').addClass('modal-body');

    var modalFooter = $('<div>').addClass('modal-footer');

    var cancelButton = null,
        actionButton = null;


    /**
     *
     * Public Methods
     */
    var setModalTitle = function(title)
    {
        modalTitle.html(title);
    };

    var setModalBody = function(body)
    {

        $(body).appendTo(modalBody);
    };

    var setCancelButton = function(cssClass, attrs, value)
    {
        cancelButton = getButton(cssClass, attrs).html(value);
        cancelButton.attr('data-dismiss', 'modal');
    };

    var setActionlButton = function(cssClass, attrs, value)
    {
        actionButton = getButton(cssClass, attrs).html(value);
    };

    var getModal = function()
    {
        _init();

        // modal Dialog
        modalContent.appendTo(modalDialog);

        // modal
        modalDialog.appendTo(modal);

        return modal;

    };


    var getOldModal = function()
    {
        _init();
        var dialog = $('<div>');

        // modal Dialog
        modalContent.appendTo(dialog);

        // modal
        dialog.appendTo(modal);

        return modal;
    };




    /**
     * Private Methods
     */

    function _init()
    {
        var attrs = {
            'type': 'button'
        };

        if( (cancelButton == null) ){

            var cancelValue = 'Cancelar';
            setCancelButton('btn-default', attrs, cancelValue);
        }

        if( (actionButton == null) ){
            var actionValue = 'Guardar';
            setActionlButton('btn-primary', attrs, actionValue);
        }

        // footer
        cancelButton.appendTo(modalFooter);
        actionButton.appendTo(modalFooter);

        //hader
        times.appendTo(closeButton);
        closeButton.appendTo(modalHeader);
        modalTitle.appendTo(modalHeader);

        // modal content
        modalHeader.appendTo(modalContent);
        modalBody.appendTo(modalContent);
        modalFooter.appendTo(modalContent);
    }

    function getButton(cssClass, attrs)
    {
        if( attrs['type'] == null )
            attrs['type'] = 'button';

        var button = $('<button>').addClass('btn '+cssClass);

        for(var attr in attrs){
            button.attr(attr, attrs[attr]);
        }

        return button;
    }

    return {
        renderModal: getModal,
        renderOldModal: getOldModal,
        setActionButton: setActionlButton,
        setCancelButton: setCancelButton,
        setBody: setModalBody,
        setTitle: setModalTitle
    };
};

