var TicketsManager = function(table, modalName, resourceUrl){

    var aero = new AeroprontoAsync();
    var modalManager = new Modal(modalName);
    var modal = null;

    var row = $('<div>').addClass('row action');
    var col = $('<div>').addClass('col-md-12 pull-right');

    var tableObject = null;

    function getAction(color, type)
    {
        if( type == null || type == undefined )
            type = 'button';

        return $('<button>').addClass('btn '+color+' btn-circle col-xs-offset-1')
            .attr('type', type);
    }

    function getIcon(iconClass)
    {
        return $('<i>').addClass('fa '+iconClass);
    }

    function compose(action, icon)
    {
        action.html(icon);
        action.appendTo(col);
        col.appendTo(row);
    }

    function showErrors(errors)
    {
        var div = $('<div>'),
            span = $('<span>').addClass('text-danger'),
            ul = $('<ul>');

        for(var error in errors){
            var li = $('<li>').html(errors[error]);
            li.appendTo(ul);
        }

        ul.appendTo(span);
        span.appendTo(div);
        div.appendTo('.panel.panel-info .panel-body');
    }

    function updateTable()
    {
        var tableObject = $(table).DataTable();
        tableObject.ajax.reload(null, false);
    }


    var getData = function(){

        var tableObject = $(table).DataTable();
        tableObject.ajax.url($(this).attr('href'));
        tableObject.ajax.reload(null, false);

    };

    var initialClientTable = function()
    {
        compose(getAction('btn-default').addClass('action'), getIcon('fa-external-link-square'));

        return {
            "lengthMenu" : [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "ajax": resourceUrl+"/get",
            "columnDefs": [ {
                "bRetrieve": true,
                "targets": -1,
                "data": null,
                "defaultContent": row.prop('outerHTML')
            } ]
        };

    };

    var setActive = function(event){

        event.preventDefault();

        $('#tickets-submenus li a').each(function(index, element){
            $(element).removeClass('active');
        });

        $(this).children().toggleClass('active');

    };

    var openTicket = function()
    {
        var ticketId = $(this).parents('tr').children().first().html();

        if( !isNaN(ticketId) ){
            window.location = resourceUrl+'/'+ticketId;
            return;
        }

        ticketId = $(this).parents('tr').prev().children().first().html();
        window.location = resourceUrl+'/'+ticketId;

    };

    var getClientTable = function()
    {
        return tableObject = $(table).DataTable(initialClientTable());
    };


    /**
     * Tiket Crud
     */
    var showModal = function()
    {
        var options = {
            url: resourceUrl+'/create',
            type: 'GET'
        };

        aero.asyncRequest(options, function(data){
            modalManager.setBody(data.data);
        });

        modalManager.setTitle('Agregar nueva reclamación');

        modal = modalManager.renderModal();
        modal.prependTo($('body'));
    };

    var storeTicket = function()
    {
        var form = $('#store-ticket');

        if( !form.isValid() )
            return;

        var options = {
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        };

        aero.asyncRequest(options, function(response){
            if( response.status == 402 )
            {
                showErrors(response.errors);
                return;
            }
            updateTable();
            modal.modal('hide');
        });
    };

    return {
        getDataAction: getData,
        setActiveAction: setActive,
        getClientTableOptions: initialClientTable,
        openTicketAction: openTicket,
        getClientTableAction: getClientTable,
        openAddTicketAction: showModal,
        storeTicketAction: storeTicket
    };
};
