var packageManager = function() {

    var targetId;
    var table = null;
    var asyncRequest = AeroprontoAsync();
    var actions = asyncRequest.modals();

    var setDataTableAction = function(html) {
        var division = $('#table-container[role=items-table]');
        division.html(html);
        console.log(html);

        // Here I initialize Items actions because the table is created here
        var itemsInit = itemsManager();
        itemsInit.init();

        if ( $.fn.dataTable.isDataTable(table)) {
            table.destroy();
        } else {
            table = $('#itemsTable').DataTable({
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
            });

            $('#itemsTable_filter input').addClass('form-control');
        }
    };

    var autoCloseNotificationsAction = function() {

        asyncRequest.closeNotification()
    };

    var setPackageId = function() {
        asyncRequest.resetForm($('form[role=addItem]'), '#error-display');
        $('#package_id')
            .val($(this).data('package'));
    };

    var setTarget = function() {
        targetId = $(this).data('package-id');
        $('#edit-package').val(targetId);
    };

    var deletePackage = function() {

        var options = {
            type: 'delete',
            dataType: 'json',
            url: $(this).data('url') + targetId
        };

        var action = function(id)
        {
            $('#delete-package').modal('hide');
            updateRow(id)
        };
        asyncRequest.asyncRequest(
            options,
            action,
            targetId
        );
        var message = 'Paquete cancelado.';
        asyncRequest.showAlert('/showalerts/danger/' + message, 'get', '#notify');
    };

    var addItems = function() {

        var errorDisplay = '#error-display';
        var data = $('form[role=addItem]');
        var options = {
            type: data.attr('method'),
            url: $(this).data('url'),
            data: data.serialize()
        };

        var onSuccess = function() {
            updateRow($('#package_id').val());
            asyncRequest
                .modals()
                .closeModal('#myModal');
        };

        asyncRequest.asyncRequest(
            options,
            asyncRequest.formValidation,
            errorDisplay,
            onSuccess
        );
    };

    var updateRow = function(id) {
        var options = {
            url: 'package/row/' + id,
            type: 'get'
        };

        asyncRequest.asyncRequest(
            options,
            actions.updateOrCreateRow,
            '#row-'+id
        );

    };

    var loadPackage = function() {

        var id = $(this).data('package');
        $('#myModalLabel').html('Editar Paquete PSDQ-' + id);

        var tableOptions = {
            url: '/items/table/' + id,
            type: 'get'
        };
        asyncRequest.asyncRequest(
            tableOptions,
            setDataTableAction
        );
    };


    /*var updateStatus = function(){

        var id = $(this).data('package');
        var options = {
            url: $(this).data('url') + id,
            data: $('#changeStatus').serialize(),
            type: $(this).data('method')
        };

        asyncRequest.asyncRequest(
            options,
            actions.updateOrCreateRow,
            '#row-' + id
        );
    };*/

    return {
        remove: deletePackage,
        setTarget: setTarget,
        add: addItems,
        setPackageId: setPackageId,
        loader: loadPackage,
        setDataTable: setDataTableAction,
        //updateStatus: updateStatus,
        autoCloseNotifications: autoCloseNotificationsAction
    };
};

var itemsManager = function(){

    var itemId;
    var asyncRequest = AeroprontoAsync();
    var actions = asyncRequest.modals();

    var setItemIdAction = function() {
        itemId = $(this).data('id');
    };

    var deleteItemsAction = function(){
        var options = {
            url: $(this).data('url') + itemId,
            type: 'DELETE'
        };

        asyncRequest.asyncRequest(
            options,
            actions.deleteRow,
            '#item-row-' + itemId,
            '#delete-item'
        );
        var message = 'artículo borrado.';
        asyncRequest.showAlert('/showalerts/danger/' + message, 'get', '#panel-notify');
    };

    var InitializeObjects = function(){

        $('#itemsTable')
            .on('click', 'button.btn-danger', setItemIdAction);

        $('#itemsTable')
            .on('click', 'button.btn-info', editItemAction);

        $('#delete-item').on('click', 'button.btn.btn-danger', deleteItemsAction);


        $('#btnEditItem').on('click', updateItemAction);
    };

    var fillEditFormAction = function(response) {
        var $form = $('form[role=editItem]');
        $form.find('input[name=name]').val(response.data.name);
        $('form[role=editItem]').find('#package_id').val(response.data.package_id);
        $form.find('input[name=weight]').val(response.data.weight);
        $form.find('textarea[name=description]').val(response.data.description);
    };

    var editItemAction = function () {

        var options = {
          url: $(this).data('url').replace('{id}', $(this).data('id')),
          type: $(this).data('type'),
          dataType: 'json'
        };

        itemId = $(this).data('id');
        asyncRequest.asyncRequest(options, fillEditFormAction);
    };

    var updateItemAction = function(){

        var $form = $('form[role=editItem]');

        var options = {
            url: $(this).data('url') + itemId,
            type: $(this).data('type'),
            data: $form.serialize()
        };

        var onSuccess = function() {

            asyncRequest.asyncRequest(
                options,
                actions.updateOrCreateRow,
                '#item-row-' + itemId
            );
            actions.closeModal('#add-item');
        };

        asyncRequest.asyncRequest (
            options,
            asyncRequest.formValidation,
            '#item-error-display',
            onSuccess
        );
    };

    return {
        init: InitializeObjects
    };
};

var AeroprontoAsync = function(){
    /***
     *
     * @param options ajax options
     * @param fn callback function
     * @param fnParams callback params
     */
    var asyncRequestAction = function(options, fn, fnParams) {

        var util = utilitiesActions();

        fnParams = util.getParameters(arguments, 2);
        //initialize();

        $.ajax(options).done(function(res){
            //console.log(res);
            if(fn) {
                if(fnParams) {
                    fnParams.push(res);
                    fn.apply(null, fnParams);
                } else {
                    fn(res);
                }
            }
        });
    };

    var utilitiesActions = function() {

        var getParametersUtility = function(paramsArray, index) {

            paramsArray = Array.prototype.slice.call(paramsArray);
            return paramsArray
                .splice(index)
        };

        return {
            getParameters: getParametersUtility
        }
    };

    var showAlertsAction = function(url, method, target) {

        var options = {
            url: url,
            type: method || 'get'
        };

        var action = function(tar, res) {
            $(tar).replaceWith(res);
        };

        asyncRequestAction(
            options,
            action,
            target
        );
        closeNotificationsAction();
    };

    var closeNotificationsAction = function() {

        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    };

    var modalsActions = function() {

        var deleteRowAction = function(rowId, modalId){
            $(rowId).remove();
            $(modalId).modal('hide');
        };

        var updateOrCreateRowAction = function(rowId, tr){
            if(tr){
                $(rowId).replaceWith(tr);
            }
        };

        var closeModalAction = function(modalId) {
            var showErrors = $('.alert-danger');

            showErrors.each(function(){
                $(this).addClass('hide');
                $('#item-error-display').html('');
            });

            $(modalId).modal('hide');
        };

        return {
            deleteRow: deleteRowAction,
            updateOrCreateRow: updateOrCreateRowAction,
            closeModal: closeModalAction
        };
    };

    var modalModel = function() {

        var _modalId;
        var _title;
        var _action;

        var construct = function(modalId, title, fnAction) {
            _modalId = modalId;
            _title = title;
            _action = fnAction;
            configureModal();
            return getModal();
        };

        var getModal = function() {
            if(_modalId){
                return _modalId.contains('#')
                    ? $(_modalId)
                    : $('#'+ _modalId);
            }
        }

        var configureModal = function() {
            var modal = getModal();
            modal.find('#myModalLabel').val(_title);
            var action = modal.find('button').last();
            action.unbind('click');
            action.on('click', _action);
        };

        return {
            modalModel: construct
        };

    };

    var formValidationAction = function(target, fn, data) {
        if(data.error) {
            for(var error in data.message) {
                for(var msg in data.message[error]){
                    $(target).append('<li>'+data.message[error][msg]+'</li>');
                }
            }
            $(target)
                .parent()
                .removeClass('hide');
        } else {
            fn();
        }
    };

    var formResetAction = function(form, placeHolder) {

        var errorPlaceHolder = $(placeHolder);
        errorPlaceHolder
            .parent()
            .addClass('hide');

        errorPlaceHolder.text('');
        form[0].reset();
    };

    return {
        asyncRequest: asyncRequestAction,
        utilities: utilitiesActions,
        modals: modalsActions,
        modelModal: modalModel,
        showAlert: showAlertsAction,
        closeNotification: closeNotificationsAction,
        formValidation: formValidationAction,
        resetForm: formResetAction
    };

};

var userManager = function() {

  var deleteTarget;

  var prepareDeleteAction = function() {
      deleteTarget = $(this).data('dst-target');
  };

   var deleteAction = function() {

       var requester = AeroprontoAsync();
       var options = {
           url: $(this).data('dst-url') + deleteTarget,
           type: 'DELETE'
       };
       requester.asyncRequest(options);

       var message = 'Usuario Borrado';
       requester.showAlert('/showalerts/danger/' + message, 'get', '#panel-notify');
       $(this).parents().find('.modal').modal('hide')
   };

    return {
        prepareDelete: prepareDeleteAction,
        deleteAction: deleteAction
    };
};
